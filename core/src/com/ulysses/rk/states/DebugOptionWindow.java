package com.ulysses.rk.states;

import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Window;

public class DebugOptionWindow extends Window{
	Skin skin;
	CheckBox checkDrawDebug, checkDisableCamMove, drawGrid, alignToGrid, camFollow;
	public DebugOptionWindow(String title, Skin skin) {
		super(title, skin);
		this.skin = skin;
		this.setSize(200, 300);
		createUI();
	}
	public boolean isDrawDebug(){
		return checkDrawDebug.isChecked();
	}
	public boolean isCamMovement(){
		return !checkDisableCamMove.isChecked();
	}
	public boolean isGridAligned(){
		return alignToGrid.isChecked();
	}
	public boolean isDrawGrid(){
		return drawGrid.isChecked();
	}
	public boolean isCamFollow(){
		return camFollow.isChecked();
	}
	private void createUI(){
		Table table = new Table();
		this.addActor(table);
		table.setFillParent(true);
		checkDrawDebug = new CheckBox("Draw B2D", skin);
		table.add(checkDrawDebug);
		table.row();
		checkDisableCamMove = new CheckBox("Disable Cam", skin);
		table.add(checkDisableCamMove);
		table.row();
		camFollow = new CheckBox("follow", skin);
		table.add(camFollow);
		
		table.row();
		alignToGrid = new CheckBox("Align to grid", skin);
		table.add(alignToGrid);
		table.row();
		drawGrid = new CheckBox("Draw grid", skin);
		table.add(drawGrid);
	}
	
}
