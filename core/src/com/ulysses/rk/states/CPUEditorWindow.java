package com.ulysses.rk.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.utils.Align;

import cpu.BoardComponent;
import cpu.CircuitBoard;
import cpu.Pin;
import cpu.Wire;
import resource.Resource;

public class CPUEditorWindow extends Window{
		
	CircuitBoard board;
	BoardComponent lastHoveredActor;
	//for dragging loose ends
	Pin mousePin = new Pin(null);
	Wire tmpWire;
	
	public CPUEditorWindow() {
		super("CPU", Resource.getInstance().getDefaultSkin());
				
		this.setResizable(true);
		this.setDebug(true);
		this.setName("CPU Win");
		this.addActor(mousePin);
		
		this.addListener(new InputListener(){
			Actor dragComponent = null;
			Actor linkComponent = null;
			/** Called when a mouse button or a finger touch goes down on the actor. If true is returned, this listener will receive all
			 * touchDragged and touchUp events, even those not over this actor, until touchUp is received. Also when true is returned, the
			 * event is {@link Event#handle() handled}.
			 * @see InputEvent */
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				Actor a = CPUEditorWindow.this.hit(x, y, true);
				if(a!=null){
					if(a.getName()!= null){
						if(a.getName().equals(CPUEditorWindow.this.getName())){
							//debug add a new component
							//KeyInputComponent c = new KeyInputComponent();
							//Vector2 v = CPUEditorWindow.this.localToDescendantCoordinates(board, new Vector2(x,y));
							//c.setPosition(x, y, Align.center);
							
							//CPUEditorWindow.this.addActor(c);
							//System.out.println("add c at " + x + " " + y + " -> " + v);
						}
						//check for Pin first, special case
						else if(a instanceof Pin){
							Pin pin = (Pin) a;
							if(!board.isWired(pin)){
								linkComponent = a;
								tmpWire = new Wire(pin, mousePin);
								CPUEditorWindow.this.addActor(tmpWire);
								System.out.println("Adding tmp Wire");
								
							} else {
								tmpWire = board.getWire(pin);
								if(tmpWire.from == pin){
									tmpWire.from = mousePin;
								} else tmpWire.to = mousePin;
								
							}
							
							return true;
						}
						// then generic Component
						else if(a instanceof BoardComponent){
							dragComponent = a;
							//board.deselectAll();
							((BoardComponent) a).setSelectionState(true);
							return true;
						}

						else {
							//ignore
						}						
					} else {
						System.out.println("border hit");
					}

					System.out.println("hit " + a.getName());
				}
				
				Gdx.app.debug("CPUEditor", "Touchdown");
				return true;
			}

			/** Called when a mouse button or a finger touch goes up anywhere, but only if touchDown previously returned true for the mouse
			 * button or touch. The touchUp event is always {@link Event#handle() handled}.
			 * @see InputEvent */
			public void touchUp (InputEvent event, float x, float y, int pointer, int button) {

				Actor a = CPUEditorWindow.this.hit(x, y, true);
				if(a instanceof Pin){
					if(a==mousePin){
						System.out.println("I knew it");
					}
					if(tmpWire!=null){
						Pin start = tmpWire.from;
						tmpWire.removeActor(start);
						board.linkComponents(start, (Pin) a);
						//CPUEditorWindow.this.addActor(board.linkComponents(tmpWire.from, (Pin) a));
						boolean succ = CPUEditorWindow.this.removeActor(tmpWire);
						System.out.println("removing tmp wire" + succ);
						
					}										
				}

				dragComponent = null;
				linkComponent = null;
				Gdx.app.debug("CPUEditor", "TouchUp");
				
			}

			/** Called when a mouse button or a finger touch is moved anywhere, but only if touchDown previously returned true for the mouse
			 * button or touch. The touchDragged event is always {@link Event#handle() handled}.
			 * @see InputEvent */
			public void touchDragged (InputEvent event, float x, float y, int pointer) {
		
				Vector2 pos = CPUEditorWindow.this.board.parentToLocalCoordinates(new Vector2(x,y));
				mousePin.setX(pos.x);
				mousePin.setY(pos.y);
				if(dragComponent!=null){
					dragComponent.setPosition(pos.x, pos.y, Align.center);
					//dragged.moveBy(x - dragged.getWidth() / 2, y - dragged.getHeight() / 2);

					
				}
			}	
			
			/** Called any time the mouse cursor or a finger touch is moved over an actor. On the desktop, this event occurs even when no
			 * mouse buttons are pressed (pointer will be -1).
			 * @param fromActor May be null.
			 * @see InputEvent */
			public void enter (InputEvent event, float x, float y, int pointer, Actor fromActor) {
				if(fromActor!=null && fromActor instanceof BoardComponent){
					lastHoveredActor = (BoardComponent) fromActor;
					lastHoveredActor.setHoverState(false);
					//System.out.println("enter " + lastHoveredActor.getName());
				}
			}

			/** Called any time the mouse cursor or a finger touch is moved out of an actor. On the desktop, this event occurs even when no
			 * mouse buttons are pressed (pointer will be -1).
			 * @param toActor May be null.
			 * @see InputEvent */
			public void exit (InputEvent event, float x, float y, int pointer, Actor toActor) {
				if(toActor instanceof BoardComponent){
					lastHoveredActor = (BoardComponent) toActor;
					lastHoveredActor.setHoverState(true);
					//System.out.println("exit " + lastHoveredActor.getName());
				}
					
			}
		});

		
	}
	public void setBoard(CircuitBoard board){
		this.clearChildren();
		this.addActor(board);
		this.board = board;
	}
		
	@Override
	public void act (float delta) {
		super.act(delta);
		if(board!=null){
			for(Wire w : board.getWires()){
				w.update();
			}
		}
	}
}
