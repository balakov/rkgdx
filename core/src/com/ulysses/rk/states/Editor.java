package com.ulysses.rk.states;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.ulysses.rk.Raumkampf;
import com.ulysses.rk.world.Universe;
import com.ulysses.rk.world.entity.AnchorPoint;
import com.ulysses.rk.world.entity.DefaultPart;
import com.ulysses.rk.world.entity.Part;
import com.ulysses.rk.world.entity.PartContainer;
import com.ulysses.rk.world.entity.Selectable;

import util.Config;

public class Editor implements Screen {

	private Raumkampf instance;
	SpriteBatch batch;
	Texture img;
	ShapeRenderer shapeRenderer = new ShapeRenderer();
	
	
	Universe universe = new Universe();
	boolean simulating = true;	
	
	//camera
	OrthographicCamera cam;
	OrthographicCamera hudCam;
	Vector2 camSpeed = new Vector2(2, 2);
	float camSensitivity = 50;
	boolean isCamDragging = false;
	Vector2 camDragVector = new Vector2();
	Vector2 oldCamPosition = new Vector2();

	// gui
	private Stage stage;
	Skin skin = new Skin(Gdx.files.internal("assets/data/uiskin.json"));
	PartConfigWindow info;
	PartChooserWindow choices;
	DebugOptionWindow debug;
	ShipOverviewWindow overview;
	CPUEditorWindow cpu;

	// selection
	Part currentContainer;
	Selectable display;
	AnchorPoint selectedAnchor;
	

	// linking
	private AnchorPoint linkStartAnchor;
	boolean isLinking = false;

	public Editor(Raumkampf instance) {
		this.instance = instance;
		batch = new SpriteBatch();
		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();

		// Constructs a new OrthographicCamera, using the given viewport width
		// and height
		// Height is multiplied by aspect ratio.
		cam = new OrthographicCamera(400, 400 * (h / w));
		// cam.translate(new Vector3(0, -200, 0));
		hudCam = new OrthographicCamera(w, h);
		createUI();
		universe.createDebugScene();

		InputMultiplexer multiplexer = new InputMultiplexer();
		multiplexer.addProcessor(stage);
		multiplexer.addProcessor(new InputAdapter() {

			@Override
			public boolean keyDown(int keycode) {
				System.out.println(Input.Keys.toString(keycode));
				if (keycode == Input.Keys.Q) {
					for (Part p : universe.getAllSelectedParts()) {
						universe.removeFromContainer(p);
					}
				}
				if (keycode == Input.Keys.DEL || keycode == Input.Keys.BACKSPACE) {
					for (Part p : universe.getAllSelectedParts()) {
						universe.deletePart(p);
					}
				
					if(selectedAnchor!=null){
						universe.unlink(selectedAnchor);
						
					}
						
				}
				if (debug.isCamMovement()) {
					if (Input.Keys.LEFT == keycode) {
						cam.translate(new Vector3(-10, 0, 0));
					} else if (Input.Keys.RIGHT == keycode) {
						cam.translate(new Vector3(10, 0, 0));
					} else if (Input.Keys.UP == keycode) {
						cam.translate(new Vector3(0, 10, 0));
					} else if (Input.Keys.DOWN == keycode) {
						cam.translate(new Vector3(0, -10, 0));
					} else if (Input.Keys.PLUS == keycode) {
						cam.zoom -= 0.2f;
					} else if (Input.Keys.MINUS == keycode) {
						cam.zoom += 0.2f;
					}
					cam.update();
				}
				return false;
			}

			@Override
			public boolean scrolled(int amount) {
				cam.zoom += amount * 0.1f;
				cam.update();
				return true;
			}

			@Override
			public boolean touchDown(int x, int y, int pointer, int button) {
				Ray ray = cam.getPickRay(x, y);
				Vector2 screenPos = new Vector2(ray.origin.x, ray.origin.y);
				Vector2 worldPos = new Vector2(screenPos.x /= Config.getScaleFactor(),
						screenPos.y /= Config.getScaleFactor());

				List<Part> parts = universe.getPartsAt(worldPos.x, worldPos.y);
				if (parts.isEmpty()) {
					Selectable addMe = (Selectable) choices.getSelectedPart();
					universe.debugAddObject(worldPos.x, worldPos.y, addMe);
					choices.deselect();
					isCamDragging = true;
					oldCamPosition.x = worldPos.x;
					oldCamPosition.y = worldPos.y;
					return true;
				}

				Gdx.app.debug("Editor", "Mouse hit at " + worldPos + " ,hitting " + parts.size() + " parts");

				if (!Gdx.input.isKeyPressed(Input.Keys.SHIFT_RIGHT)) {
					universe.selectAllParts(false);
				}

				// part not get(0)?
				Part top = parts.get(0);
				Gdx.app.debug("Editor", top.getPartName() + " was hit");
				info.setPart(top);
				Selectable s = (Selectable) top;
				AnchorPoint anchor = top.getAnchorAtWorldPos(worldPos);

				currentContainer = top;
				
				PartContainer active = universe.getPartContainer(currentContainer);
				if (active != null) {
					System.out.println("Selected PartContainer " + active.toString());
					overview.setShip(active);
					cpu.setBoard(active.getCPU());
				} 

				if (anchor == null) {
					ArrayList<Part> selection = new ArrayList<Part>();
					selection.addAll(universe.getAllSelectedParts());
					selection.add(top);

					universe.initSoftMove(top, worldPos);

				} 
				// an anchor was clicked, should switch to link mode
				else
				{
					s = anchor;
					selectedAnchor = anchor;
					linkStartAnchor = (AnchorPoint) s;
					isLinking = true;
				}

				
				
				if (s.isSelected()) {
					System.out.println("Deselecting: " + top.getPartName());
					s.setSelectionState(false);
				} else {
					System.out.println("Selecting: " + top.getPartName());
					if (!Gdx.input.isKeyPressed(Input.Keys.SHIFT_RIGHT)) {
						universe.selectAllParts(false);
					}
					s.setSelectionState(true);
				}

				return true; // return true to indicate the event was handled
			}

			@Override
			public boolean touchUp(int x, int y, int pointer, int button) {

				Ray ray = cam.getPickRay(x, y);
				Vector2 worldPos = new Vector2(ray.origin.x, ray.origin.y);


				universe.endSoftMove();
				isLinking = false;
				isCamDragging = false;
				
				if (linkStartAnchor != null) {
					System.out.println("Link mode: checking target");
					List<Part> hits = universe.getPartsAt(worldPos.x, worldPos.y);
					if (!hits.isEmpty()) {
						System.out.println("Something was hit");
						if (hits.get(0) instanceof Part) {
							Part target = (Part) hits.get(0);
							AnchorPoint linkEndAnchor = target.getAnchorAtWorldPos(worldPos);
							if (linkEndAnchor != null) {
								universe.linkParts(linkStartAnchor, linkEndAnchor);
							} else {
								System.out.println("There is no AnchorPoint here.");
								
							}
						}

					}
					else {
						Gdx.app.debug("Editor", "Adding virtual part for link endpoint");
						DefaultPart vEnd = new DefaultPart();
						AnchorPoint a = vEnd.getTrussAnchors().get(0);
						universe.debugAddObject(worldPos.x, worldPos.y, vEnd);
						
						universe.linkParts(linkStartAnchor, a);
						
						
					}
				}
				linkStartAnchor = null;
				return true; // return true to indicate the event was handled
			}

			@Override
			public boolean touchDragged(int screenX, int screenY, int pointer) {
				Ray ray = cam.getPickRay(screenX, screenY);
				Vector2 worldPos = new Vector2(ray.origin.x, ray.origin.y);
				universe.updateSoftMove(worldPos);
				
				if(isLinking){
					
					
					
				}
				
				if(isCamDragging){
					Gdx.app.debug("Editor", "Dragging camera");
					camDragVector.x = oldCamPosition.x - worldPos.x;
					camDragVector.y = oldCamPosition.y - worldPos.y;
					oldCamPosition.x = worldPos.x;
					oldCamPosition.y = worldPos.y;
					
					cam.translate(camDragVector.x, camDragVector.y);
					cam.update();
				}
				
				return false;
			}

			@Override
			public boolean mouseMoved(int screenX, int screenY) {
				Ray ray = cam.getPickRay(screenX, screenY);
				Vector2 worldPos = new Vector2(ray.origin.x, ray.origin.y);
				worldPos.x /= Config.getScaleFactor();
				worldPos.y /= Config.getScaleFactor();


				
				List<Part> parts = universe.getPartsAt(worldPos.x, worldPos.y);
				universe.hoverAllParts(false);
				if (!parts.isEmpty()) {
					for (Selectable p : parts) {
						if (p != null) {
							if (p instanceof Part) {
								Part part = (Part) p;
								AnchorPoint a = part.getAnchorAtWorldPos(worldPos);
								if (a != null) {
									a.hoveredOn = true;
								}
							} else {
								p.setHoverState(true);
							}
						} else
							System.out.println("Memory fault");

					}
				}

				return false;
			}

		});
		Gdx.input.setInputProcessor(multiplexer);
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void render(float delta) {

		debugPlayerInput();

		stage.act(Gdx.graphics.getDeltaTime());

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();

		if (debug.isCamFollow()) {
			if(currentContainer!=null){
				zoomTo(currentContainer);
			}
		}


		
		if (simulating)
			universe.doPhysicsStep(0.01f);

		if (debug.isDrawDebug())
			universe.debugRender(cam);

		stage.draw();
		universe.lineRender(cam);

		batch.end();
		


		batch.begin();
		hudCam.update();
		batch.setProjectionMatrix(hudCam.combined);
		instance.defaultFont.setColor(1f, 1f, 1, 1); 
		instance.defaultFont.draw(batch, "fps: " + Gdx.graphics.getFramesPerSecond(), -100, 20);
		instance.defaultFont.draw(batch, "bodies: " + universe.getNumberOfBodies(), -100, 40);
		instance.defaultFont.draw(batch, "joints: " + universe.getNumberOfJoints(), -100, 60);
		instance.defaultFont.draw(batch, "container: " + universe.getPartContainer().values().size(), -100, 80);
		batch.end();

		batch.begin();
		batch.setProjectionMatrix(cam.combined);
		universe.draw(batch, 1);
		batch.end();
		
		if(isLinking){
			shapeRenderer.begin(ShapeType.Line);
			shapeRenderer.setColor(Color.WHITE);
			Vector2 start = linkStartAnchor.getWorldPosition();
			Vector3 scr_start = cam.project(new Vector3(start.x,start.y,1));
			Vector2 scr_end = new Vector2(Gdx.input.getX(),Gdx.graphics.getHeight()-(Gdx.input.getY()));
			shapeRenderer.line(scr_start.x, scr_start.y, scr_end.x, scr_end.y);
			shapeRenderer.end();
		}
	}

	@Override
	public void resize(int width, int height) {

		stage.getViewport().update(width, height, true);
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		batch.dispose();
		img.dispose();
		stage.dispose();
	}

	private void createUI() {
		stage = new Stage();

		// Add widgets to the table here.
		final TextButton sim = new TextButton("stop sim", skin);
		sim.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				if (simulating)
					sim.setText("start sim");
				else
					sim.setText("stop sim");
				simulating = !simulating;
			}
		});

		stage.addActor(sim);

		// Add widgets to the table here.
		final TextButton read = new TextButton("read", skin);
		read.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				universe.getPartContainer(currentContainer).readFromFile(universe);
			}
		});
		read.setPosition(100, 0);
		// Add widgets to the table here.
		final TextButton write = new TextButton("write", skin);
		write.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				universe.getPartContainer(currentContainer).writeToFile();
			}
		});
		write.setPosition(150, 0);
		stage.addActor(read);
		stage.addActor(write);

		final TextButton clear = new TextButton("clear", skin);
		clear.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				universe.deleteAll();
			}
		});
		clear.setPosition(220, 0);
		stage.addActor(clear);
		info = new PartConfigWindow("Info", skin);
		stage.addActor(info);

		choices = new PartChooserWindow("Parts", skin);
		stage.addActor(choices);

		debug = new DebugOptionWindow("Debug", skin);
		stage.addActor(debug);

		cpu = new CPUEditorWindow();
		stage.addActor(cpu);

		overview = new ShipOverviewWindow(skin);
		overview.setShip((universe.getPartContainer(currentContainer)));
		overview.setRound(true);
		stage.addActor(overview);
		layoutWindows();

	}

	private void layoutWindows() {
		info.setPosition(stage.getWidth() - info.getWidth(), stage.getHeight() - info.getHeight());
		choices.setPosition(0, stage.getHeight() - choices.getHeight());
		overview.setPosition(choices.getWidth(), stage.getHeight() - overview.getHeight());
		debug.setPosition(stage.getWidth() - debug.getWidth(), 0);
	}

	private void debugPlayerInput() {
		ArrayList<Integer> pressed = new ArrayList<Integer>();
		for (Integer key : Config.getAllowedKeys()) {
			if (Gdx.input.isKeyPressed(key))
				pressed.add(key);
		}
		PartContainer player = universe.getPartContainer(currentContainer);
		if (player != null) {
			player.signal(pressed);
		}

	}
	private void zoomTo(Part part){
		Vector2 partPosition = part.getWorldPosition();
		Gdx.app.debug("Editor", "Zooming to " + part + ", rotation " + part.getWorldAngle());
		this.cam.position.x = partPosition.x;
		this.cam.position.y = partPosition.y;
		float camAngle = -getCameraCurrentXYAngle(cam) + 180;
		float partangle = (float) Math.toDegrees(part.getWorldAngle());
		cam.rotate((camAngle-partangle)+180);
		
		//this.cam.zoom = 0.01f;
		cam.update();		
	}
	public float getCameraCurrentXYAngle(OrthographicCamera cam)
	{
	    return (float)Math.atan2(cam.up.x, cam.up.y)*MathUtils.radiansToDegrees;
	}

	private void updateCam() {
		float px = Gdx.input.getX();
		float py = Gdx.input.getY();
		Vector2 ct = new Vector2();
		System.out.println(px + " + " + py);
		if (px < camSensitivity) {
			ct.x = -camSpeed.x;
		}
		if (px > Gdx.graphics.getWidth() - camSensitivity) {
			ct.x = camSpeed.x;
		}
		if (py < camSensitivity) {
			ct.y = camSpeed.y;
		}
		if (py > Gdx.graphics.getHeight() - camSensitivity) {
			ct.y = -camSpeed.y;
		}
		cam.translate(ct);
		cam.update();

	}
}
