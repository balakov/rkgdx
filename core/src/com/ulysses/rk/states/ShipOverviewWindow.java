package com.ulysses.rk.states;

import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextArea;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.ulysses.rk.world.entity.PartContainer;

public class ShipOverviewWindow extends Window {

	private PartContainer container;

	public ShipOverviewWindow(Skin skin) {
		super("Overview", skin);
		this.setKeepWithinStage(true);
		this.setResizable(true);
	}
	
	
	public void setShip(PartContainer container)
	{
		if(container!=null){
			if(this.container!=container){
				this.container = container;
				this.reset();
				this.add(container.getInfoWidgets());
				
			}
		}
	}
}
