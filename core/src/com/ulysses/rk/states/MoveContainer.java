package com.ulysses.rk.states;

import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;
import com.ulysses.rk.world.entity.Part;

public class MoveContainer {
	ArrayList<Part> movingParts;
	ArrayList<Vector2> offsets;
	
	public void setUp(ArrayList<Part> parts, Vector2 pointerStart){
		movingParts = parts;				
		generateOffsets(pointerStart);
	}
	
	private void generateOffsets(Vector2 pointerStart){
		//convert to b2world coordinates

		offsets = new ArrayList<Vector2>();
		for(Part p : movingParts){
			offsets.add(new Vector2(pointerStart.x -  p.getContainerPosition().x, pointerStart.y - p.getContainerPosition().y));
		}
		
	}
	
	public ArrayList<Vector2> getOffsets(){
		return offsets;
	}
	
	public ArrayList<Part> getParts(){
		return movingParts;
	}
}
