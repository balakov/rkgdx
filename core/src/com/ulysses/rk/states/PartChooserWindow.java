package com.ulysses.rk.states;

import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.ulysses.rk.world.entity.Cockpit;
import com.ulysses.rk.world.entity.DefaultPart;
import com.ulysses.rk.world.entity.DefaultThruster;
import com.ulysses.rk.world.entity.Selectable;
import com.ulysses.rk.world.entity.Tank;

public class PartChooserWindow extends Window {

	List all;
	public PartChooserWindow(String title, Skin skin) {
		super(title, skin);
		String[] items = {"DefaultPart", "Truss", "Cockpit", "Default Thruster", "Tank"};
		all = new List(skin);
		all.setItems(items);
		this.add(all);
	}
	public void deselect(){
		all.setSelectedIndex(-1);
		
	}
	public Selectable getSelectedPart(){
		String selected = (String) all.getSelected();
		if(selected==null)
			return null;
		else if (selected.equals("DefaultPart")){
			return new DefaultPart();
		}
		else if(selected.equals("Cockpit")){
			return new Cockpit();
		}
		else if(selected.equals("Default Thruster")){
			return new DefaultThruster();
		}
		else if(selected.equals("Tank")){
			return new Tank();
		}
		else return null;
	}
	
//	public addChoice(Part p){
//		all.setItems(newItems);
//	}
}
