package com.ulysses.rk.states;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextArea;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.ulysses.rk.world.entity.Part;

import util.Config;

public class PartConfigWindow extends Window {

	Skin skin;
	Part part;
	TextArea nameArea;
	Table table;
	Button rotateLeft, rotateRight;
	List<CheckBox> inputs = new ArrayList<CheckBox>();
	
	public PartConfigWindow(String title, Skin skin) {
		super(title, skin);
		this.skin = skin;
		this.setSize(200, 400);
		createUI();
	}
	
	public void setPart(Part part){
		if(part != null && this.part != part){		
			this.clearChildren();
			this.part = part;
			Table configWidgets = part.getConfigWidgets();		
			if(configWidgets != null){
				this.add(configWidgets);
			}
		}
	}
	
	public void createUI(){
		table = new Table();
		
		this.add(table);
		table.setFillParent(true);
		table.setDebug(true);
		
		Label nameLabel = new Label("Name: ", super.getSkin());
		table.add(nameLabel).size(50);
		
		nameArea = new TextArea("No Part selected", super.getSkin());
		table.add(nameArea).expandX().colspan(2);
		
		table.row();
		
		/*
		Label rotationLabel = new Label("Rotate: ", super.getSkin());
		table.add(rotationLabel).size(50);
		rotateLeft = new TextButton("-10", super.getSkin());
		rotateLeft.addListener(new ChangeListener(){
			
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if(part != null){
					//part.bodylink.setTransform(part.bodylink.getPosition(), part.bodylink.getAngle()- (float) Math.toRadians(-10));
				}				
			}
		});
		table.add(rotateLeft);
		
		rotateRight = new TextButton("+10", super.getSkin());
		rotateRight.addListener(new ChangeListener(){
			
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if(part != null){
					//part.bodylink.setTransform(part.bodylink.getPosition(), part.bodylink.getAngle()- (float) Math.toRadians(10));
				}				
			}
		});		
		table.add(rotateRight);
		createInputConfig();*/
	}
	
	private void createInputConfig(){
		boolean swap = false;
		table.row();
		for(Integer keycode : Config.getAllowedKeys()){
			CheckBox c = new CheckBox(Input.Keys.toString(keycode), skin);
			c.addListener(new ChangeListener(){


				@Override
				public void changed(ChangeEvent event, Actor actor) {
					writeInputConfig();
					System.out.println("changed");
				}});
			c.setProgrammaticChangeEvents(false);
			inputs.add(c);
			table.add(c);
			if(swap)
				table.row();
			swap = !swap;
		}
	}
	
	private void readInputConfig(){
		if (part!=null) {
			System.out.println("Reading"+ part.getListenForKeys());
			
			for (CheckBox c : inputs) {
				c.setChecked(false);
			}
			for(Integer keycode : part.getListenForKeys()){
				for (CheckBox c : inputs) {
					if(c.getLabel().getText().toString().equals(Input.Keys.toString(keycode)))
						
						c.setChecked(true);
						
				} 				
				
			}
			
		}
	}
	
	private void writeInputConfig(){
		if (part!=null) {
			ArrayList<Integer> listenOn = new ArrayList<Integer>();
			for (CheckBox c : inputs) {
				if (c.isChecked()) {
					System.out.println(c.getLabel().getText());
					listenOn.add(Input.Keys.valueOf(c.getLabel().getText().toString()));
				}
			}
			System.out.println("Writing " + listenOn);
			this.part.setListenOn(listenOn);
		}
	}
}
