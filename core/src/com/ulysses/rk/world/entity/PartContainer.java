package com.ulysses.rk.world.entity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.jgrapht.Graph;
import org.jgrapht.alg.ConnectivityInspector;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.Multigraph;
import org.jgrapht.graph.SimpleGraph;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.ulysses.rk.world.Universe;

import cpu.CircuitBoard;
import resource.Resource;

public class PartContainer {
	
	public Body root;
	CircuitBoard circuitBoard;
	
	public String name;
	HashMap<UUID, Part> parts = new HashMap<UUID, Part>();
	ArrayList<Link>links = new ArrayList<Link>();
	
	//Info widgets
	Table table;
	Label lPosition, vPosition, lRotation, vRotation, lMass, vMass, lDryMass, vDryMass, lVelocity, vVelocity;
	//debug widgets
	Label lNumLinks, vNumLinks;
	
	
	
	public PartContainer(Part rootPart, Body rootbody){
		circuitBoard = new CircuitBoard();
		parts.put(rootPart.id, rootPart);
		root = rootbody; 
		rootbody.setUserData(this);
		rootPart.setPositionInContainer(new Vector2());
		for(Fixture f : root.getFixtureList()){
			f.setUserData(rootPart);
		}
		Gdx.app.debug("PartContainer", "Creating root body");
	}
	
	public boolean containsPart(UUID id){
		
		if(parts.containsKey(id))
			return true;
		else return false;
	}
	
	public void addPart(Part part, Body body){
		parts.put(part.id, part);
		Vector2 pworld = body.getPosition().cpy();
		Vector2 localPosition = root.getLocalPoint(pworld).cpy();

		part.setPositionInContainer(localPosition);
		part.setContainerPosition(root.getPosition().cpy());
		part.setContainerAngle(root.getAngle());
		

		float angle =  body.getAngle() - root.getAngle();
		part.setAngleInContainer(angle);
		for(FixtureDef f : part.getFixtureDefs(localPosition, angle)){
			Fixture fixture = root.createFixture(f);
			fixture.setUserData(part);
		}		
		Gdx.app.debug("PartContainer", "added " + part.id);
		
		// update cpu
		
		circuitBoard.addOutputAdapter(part.getCPUAdapter());
		
	}
	
	public void removePart(Part part){
		if(parts.remove(part.id)==null){
			Gdx.app.debug("PartContainer", "Error removing part");
			return;
		}
		List<Link> toRemove = new ArrayList<Link>();
		for(Link l : links){
			if(l.a.getParentPart()==part || l.b.getParentPart() ==part){
				toRemove.add(l);
				
			}
		}
		Gdx.app.debug("PartContainer", "Removing "+toRemove.size()+ " link(s)");
		links.removeAll(toRemove);
		
		List<Fixture> fixRemove = new ArrayList<Fixture>();
		for(Fixture f : root.getFixtureList()){
			if(f.getUserData() == part){
				fixRemove.add(f);
			}
		}
		Gdx.app.debug("PartContainer", "Destroying " + fixRemove.size()+ " fixture(s)");
		for(Fixture f : fixRemove){
			root.destroyFixture(f);				
		}

		circuitBoard.removeBuiltIn(part.id);
		part.setPositionInContainer(new Vector2());
		part.setContainerPosition(new Vector2());
		part.setContainerAngle(0);
		part.setAngleInContainer(0);
	}
	
	public void addLink(Link l){
		Gdx.app.debug("PartEditor", "Link added from " + l.a.getParentPart() + " to " + l.b.getParentPart());
		links.add(l);
	}
	
	// removal of a link may trigger fragmentation
	public boolean removeLink(AnchorPoint a){
		ArrayList<Link> r = new ArrayList<Link>();
		System.out.println(links.size() + " num Anchors");
		for(Link l : links){
			if(l.a == a || l.b == a){
				r.add(l);
				System.out.println("Anchor found");
			}
		}
		if(!r.isEmpty()){
			links.removeAll(r);
			return false;
		}
		return true;
	}
	

	public String toString(){
		String result = name + "NumParts: " + parts.size();
		for(Part p : parts.values())
		{
			result+="\n"+p.getPartName();
		}
		return result;
	}
	
	public Vector2 getCenter(){
		return root.getPosition();
	}
	public Collection<Part> getAllParts(){
		return parts.values();
	}
	public ArrayList<Link> getLinks(){
		return links;
	}
	
	public CircuitBoard getCPU(){
		return circuitBoard;
	}
	public AnchorPoint getAnchorPoint(Vector2 worldPoint){
		Vector2 local = root.getLocalPoint(worldPoint);
		AnchorPoint result = null;
		for(Part p : parts.values()){
			result = p.getAnchorAtWorldPos(worldPoint);
			if(result != null)
				return result;
		}
		return result;
	}
	public void update(){

		for(Fixture f : root.getFixtureList()){
			
			
			Part p = (Part) f.getUserData();
			
			p.setContainerPosition(root.getPosition());
			p.setContainerAngle(root.getAngle());

			p.update();

		}	
		
		for(Part p : parts.values()){
			Vector2 force = p.getForceEmitted(0);
			if(force.len()>1){
				float angle = (float) (Math.toDegrees(p.getContainerAngle()+p.getAngleInContainer()));
				Vector2 worldCoord = p.localToWorld(new Vector2());
				
				force.rotate(angle);
				root.applyForce(force, worldCoord, true);
			}
		}
		
		updateInfoWidgets();
	}
	
	public void signal(ArrayList<Integer> inputs){
		circuitBoard.setInput(inputs);	
		for(UUID partid : parts.keySet()){
			if(circuitBoard.getSignal(partid)!=0)
				parts.get(partid).signal(1);
			else parts.get(partid).signal(0);
		}
		
	}
	
	public void lineRender(ShapeRenderer renderer){
		for(Part p : parts.values()){
			p.drawLines(renderer);
		}
		
		for(Link l : links){
			l.drawLines(renderer);
		}
	}
	

	
	public void render(SpriteBatch batch){
		for(Part p : parts.values()){
			p.draw(batch, 0);
		}
		
		for(Link l : links){
			l.draw(batch, 0);
		}
	}

	public Table getInfoWidgets(){
		if(table==null){
			createInfoWidgets();
		}
		return table;
		
	}
	private void createInfoWidgets(){
		table = new Table();
		table.setDebug(true);
		Skin skin = Resource.getInstance().getDefaultSkin();
		
		lPosition = new Label("Position", skin);
		vPosition = new Label("", skin);
		table.add(lPosition); table.add(vPosition);table.row();
		
		lVelocity= new Label("Velocity", skin);
		vVelocity= new Label("", skin);
		table.add(lVelocity); table.add(vVelocity); table.row();
		
		lRotation= new Label("Rotation", skin);
		vRotation= new Label("", skin);
		table.add(lRotation); table.add(vRotation); table.row();
		
		lMass= new Label("Mass", skin);
		vMass= new Label("", skin);
		table.add(lMass); table.add(vMass); table.row();
		
		lDryMass= new Label("DryMass", skin);
		vDryMass= new Label("", skin);
		table.add(lDryMass); table.add(vDryMass); table.row();
		
		//debug
		lNumLinks = new Label("#Links", skin);
		vNumLinks = new Label("", skin);
		table.add(lNumLinks); table.add(vNumLinks); table.row();
		
	}
	
	public void updateInfoWidgets(){
		if(table==null){
			createInfoWidgets();
		}
		vPosition.setText(util.Math.toRoundedString(root.getPosition()));

		vVelocity.setText(util.Math.toStringDegrees(root.getLinearVelocity().len()));
		vRotation.setText(util.Math.toStringDegrees(root.getAngle()));
		int mass = MathUtils.round(root.getMass());
		
		vMass.setText((new Integer(mass).toString()));
		vDryMass.setText("N/A");
		
		// debug
		vNumLinks.setText("" + this.links.size());
		
	}
	
	
	public void writeToFile(){

	}
	
	public void readFromFile(Universe universe){
		FileHandle from = Gdx.files.local("assets/data/save/container.dat");

		JsonFactory jFactory = new JsonFactory();
		JsonParser parser;
		try {
			parser = jFactory.createParser(from.readString());
			while(!parser.isClosed()){
			    JsonToken jsonToken = parser.nextToken();
			    
			    if(jsonToken == JsonToken.START_OBJECT){
			    	readPart(parser, universe);
			    	
			    }
 
			   
			}
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Read complete!");


	}
	//creates and adds part
	private void readPart(JsonParser parser, Universe u) throws IOException{
		System.out.println("Creating part");
    	String partname = null;
    	float x = 0,y = 0, angle = 0;
    	JsonToken jsonToken = parser.nextToken();
    	if(parser.getText().equals("name")){
    		jsonToken = parser.nextToken();
    		partname = parser.getText();
    		jsonToken = parser.nextToken(); //'id'
    		jsonToken = parser.nextToken(); // val
    		UUID id = UUID.fromString(parser.getText());
    		jsonToken = parser.nextToken(); //'x'
    		jsonToken = parser.nextToken(); // val
    		x = parser.getFloatValue();
    		parser.nextToken(); //y
	    	parser.nextToken(); //val
	    	y = parser.getFloatValue();
    		parser.nextToken(); //angle
	    	parser.nextToken(); //val
	    	angle = parser.getFloatValue();
	    	System.out.println(id);
	    	/*
	    	Part p = null;
	    	if(partname.equals("Cockpit")){
	    		Cockpit c = new Cockpit();
	    		c.id = id;
	    		this.addPart(c, new Vector2(x,y));
	    	}
	    	else if(partname.equals("Tank")){
	    		Tank t = new Tank();
	    		t.id = id;
	    		this.addPart(t, new Vector2(x,y));
	    	}
	    	else if(partname.equals("Default Thruster")){
	    		DefaultThruster th = new DefaultThruster();
	    		th.readConfigFromJson(parser);
	    		th.id = id;
	    		this.addPart(th, new Vector2(x,y));
	    	}
	    	else if(partname.equals("DefaultPart")){
	    		DefaultPart dp = new DefaultPart();
	    		dp.id = id;
	    		this.addPart(dp, new Vector2(x,y));
	    	}

	    	else if(partname.equals("Link")){
	    		UUID partA, partB;
	    		Vector2 anchorPos0 = new Vector2();
	    		Vector2 anchorPos1 = new Vector2();
		    	parser.nextToken(); 
		    	parser.nextToken();
		    	partA = UUID.fromString(parser.getText());
		    	System.out.println("Loaded link Part a, id: " +partA);
	    		parser.nextToken();
		    	parser.nextToken(); 
		    	partB = UUID.fromString(parser.getText());
		    	System.out.println("Loaded link Part b, id: " +partB);
		    	parser.nextToken();
		    	parser.nextToken(); 
		    	anchorPos0.fromString(parser.getText());
		    	System.out.println(anchorPos0);
		    	parser.nextToken();
		    	parser.nextToken(); 
		    	anchorPos1.fromString(parser.getText());		    	
		    	System.out.println(anchorPos1);
		    	ComplexPart p0 = (ComplexPart)parts.get(partA);
		    	ComplexPart p1 = (ComplexPart)parts.get(partB);
		    	System.out.println(p0);
		    	System.out.println(p1);
		    	if(p0 == null || p1 == null)
		    		return;
		    	AnchorPoint a0 = p0.getAnchorAt(anchorPos0);
		    	AnchorPoint a1 = p1.getAnchorAt(anchorPos1);
		    	u.linkParts(a0, a1);
		    	return;
	    		
	    	}*/
    	}

		
	}

}
