package com.ulysses.rk.world.entity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import cpu.PartAdapter;
import resource.Resource;
import util.Config;

public class DefaultThruster extends Part {
	TextureAtlas particleAtlas; //<-load some atlas with your particle assets in
	ParticleEffect effect = new ParticleEffect();
	int lastSignal = 0;
	boolean fire = false;
	PolygonShape shape = new PolygonShape();
	
	int temperature;
	
	//Config Widgets
	//debug
	Label lPositionInContainer, lAngleInContainer, lContainerPosition, lContainerAngle,
	vPositionInContainer, vAngleInContainer, vContainerPosition, vContainerAngle;
	//temp
	Label lTemperature, vTemperature;
	
	public DefaultThruster(){

		this.anchors.add(new AnchorPoint(AnchorPoint.TYPE.Truss,new Vector2(0,-1f) , 0.5f, this));

		this.anchors.add(new AnchorPoint(AnchorPoint.TYPE.Truss,new Vector2(0,1f) , 0.5f, this));
		
		
		
		effect.load(Gdx.files.internal("assets/fire.p"),Gdx.files.internal("assets/"));
		effect.scaleEffect(0.05f);

		this.widgetTable = createConfig();
		
		cpuAdapter = new PartAdapter(this.id);
		
	}
	
	@Override
	public void draw(SpriteBatch batch, float delta) {

		float angle = (float) (Math.toDegrees(this.getContainerAngle()+this.getAngleInContainer()))-90;
		Vector2 worldCoord = this.localToWorld(new Vector2());
		
		for (ParticleEmitter e : effect.getEmitters()){
			e.getAngle().setHigh((angle));
			e.getAngle().setLow((angle));
			

		}
		
		effect.setPosition(worldCoord.x, worldCoord.y);
	
		if(fire){
			
			effect.start();
			for (ParticleEmitter e : effect.getEmitters()){
				e.getAngle().setHigh((angle));
				e.getAngle().setLow((angle));
				

			}
			effect.draw(batch, Gdx.graphics.getDeltaTime());
		} else {
			effect.allowCompletion();
			for (ParticleEmitter e : effect.getEmitters()){
				e.getAngle().setHigh((angle));
				e.getAngle().setLow((angle));
				

			}
		}
		effect.draw(batch, Gdx.graphics.getDeltaTime());
	}

	@Override
	public void drawLines(ShapeRenderer renderer) {
		super.drawLines(renderer);
		PolygonShape s = getShape();
		renderer.begin(ShapeType.Line);
		
		Color color = Config.getNormalColor();
		if(this.isSelected()){
			color = Config.getSelectedColor();
		}
		if(this.isHoveredOn()){
			color = Config.getHoverColor();
		}		
		renderer.setColor(color);
		for(int i = 0; i < s.getVertexCount(); i++){
			Vector2 from = new Vector2();
			Vector2 to = new Vector2();
			s.getVertex(i, from);
			
			if(i==s.getVertexCount()-1)
				s.getVertex(0, to);
			else s.getVertex(i+1, to);
			
			from = localToWorld(from.cpy());
			to = localToWorld(to.cpy());

			
			renderer.line(from, to);
			
		}
		renderer.end();
		drawAnchors(renderer);
	}
	@Override
	public int signal(int signal){
		if(signal!=0){
			fire = true;
			temperature+=10;
		}
		else fire = false;
		return 0;
	}
	
	@Override
	public PartAdapter getCPUAdapter(){
		
		return cpuAdapter;
	}
	
	@Override 
	public Table getConfigWidgets(){
		
		return this.widgetTable;
	}
	
	private void createConfigWidgets(){
		Skin skin = Resource.getInstance().getDefaultSkin();
		
		//debug
		lPositionInContainer = new Label("PosInCon", skin);
		
		lAngleInContainer = new Label("AnInCon", skin);
		lContainerPosition = new Label("ConPos", skin);
		lContainerAngle = new Label ("ConAngle", skin);
		vPositionInContainer = new Label("", skin);
		vAngleInContainer = new Label("", skin);
		vContainerPosition = new Label("", skin);
		vContainerAngle = new Label ("", skin);
		
		//info
		lTemperature = new Label("Temp", skin);
		vTemperature = new Label("",skin);
		
		
	}
	
	private Table createConfig(){
		if(lPositionInContainer==null){
			createConfigWidgets();
		}
		Table t = new Table();
		t.setDebug(true);
		t.align(Align.right);
		Label l = new Label("DefaultThruster", Resource.getInstance().getDefaultSkin());
		t.add(l);
		t.row();
		t.add(lTemperature);
		t.add(vTemperature);
		

		t.row();
		t.add(lPositionInContainer); t.add(vPositionInContainer);
		t.row();
		t.add(lAngleInContainer); t.add(vAngleInContainer);
		t.row();
		t.add(lContainerPosition); t.add(vContainerPosition);
		t.row();
		t.add(lContainerAngle); t.add(vContainerAngle);
		
		
		
		
		return t;
		
	}
	public void update(){
		if(temperature>0) temperature--;
		//info
		vTemperature.setText(" " + this.temperature +"°");
		
		//debug
		vPositionInContainer.setText(util.Math.toRoundedString(this.getPositionInContainer()));
		vAngleInContainer.setText(util.Math.toStringDegrees(this.getAngleInContainer()));
		vContainerPosition.setText(util.Math.toRoundedString(this.getContainerPosition()));
		vContainerAngle.setText(util.Math.toStringDegrees(getContainerAngle()));		
		
		//cpu
		if(cpuAdapter.isHoveredOn()){
			this.setHoverState(true);
		}
		
	}
	
	@Override
	public Vector2 getForceEmitted(float deltaT){
		if(fire){
			return new Vector2(0,100);
		}	
		return new Vector2(0,0);
	}
	
	public List<FixtureDef> getFixtureDefs(Vector2 offset, float angleOffset) {

		Vector2[] vs = new Vector2[4];
		vs[0] = new Vector2(-0.5f,2f);
		vs[1] = new Vector2(-1f,-2f);
		vs[2] = new Vector2(1f,-2f);
		vs[3] = new Vector2(0.5f,2f);
		
		
		
		for(int i=0; i < 4; i++){			
			vs[i].rotate((float)Math.toDegrees(angleOffset));
			vs[i].add(offset);
			
		}
		shape.set(vs);

		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = shape;
		fixtureDef.density = 0.5f; 
		fixtureDef.friction = 0.4f;
		fixtureDef.restitution = 0.6f; 
		
		List<FixtureDef> fd = new ArrayList<FixtureDef>();
		fd.add(fixtureDef);
		return fd;
	}
	
	private PolygonShape getShape(){
		PolygonShape s = new PolygonShape();
		Vector2[] vs = new Vector2[4];
		vs[0] = new Vector2(-0.5f,2f);
		vs[1] = new Vector2(-1f,-2f);
		vs[2] = new Vector2(1f,-2f);
		vs[3] = new Vector2(0.5f,2f);
		s.set(vs);
		return s;
	}
	
	public String getPartName(){
		return "Default Thruster";
	}
	
	@Override
	public void readConfigFromJson(JsonParser parser) throws IOException{
		Gdx.app.debug("Default Thruster", "Reading Config");
		System.out.println("arraytest " + parser.nextValue());
		while(parser.nextToken() != JsonToken.END_ARRAY){
			this.listenOn.add((Integer) parser.getNumberValue());
			
		}
				
		
	}
	
	@Override
	public void writeToJSON(JsonGenerator jsonGenerator, boolean delimit) throws IOException{
		Gdx.app.debug("Default Thruster", "Writing Config");
		
		if(delimit)
			jsonGenerator.writeStartObject();
		super.writeToJSON(jsonGenerator, false);
		jsonGenerator.writeArrayFieldStart("activators");
		for(Integer i : this.listenOn){
			jsonGenerator.writeNumber(i);
		}
		jsonGenerator.writeEndArray();	
		if(delimit)
			jsonGenerator.writeEndObject();
		
	}
	

	
}
