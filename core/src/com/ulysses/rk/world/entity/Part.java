package com.ulysses.rk.world.entity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;

import cpu.PartAdapter;
import util.Config;

/**
 * @author ulysses
 *
 */
/**
 * @author ulysses
 *
 */
public abstract class Part implements PhysicsComponent, RenderComponent, Selectable{
	
	public UUID id;
	
	//spatial information	
	//public Body bodylink;
	private Vector2 worldPosition;
	private float worldAngle;
	private Vector2 positionInContainer;
	private float angleInContainer;
	private Vector2 containerPosition;
	private float containerAngle;
	
	//input
	List<Integer> listenOn = new ArrayList<Integer>();
	private boolean hoveredOn = false;
	private boolean selected = false;
	
	//anchors
	List<AnchorPoint> anchors = new ArrayList<AnchorPoint>(); 	
	float anchorSize = 0.5f;
	
	//Config
	Table widgetTable = new Table();
	
	//cpu
	PartAdapter cpuAdapter;
	
	@Override
	public abstract List<FixtureDef> getFixtureDefs(Vector2 offset, float angleOffset);

	public Part(){
		id = UUID.randomUUID();
		worldPosition = new Vector2();
		positionInContainer = new Vector2();
		containerPosition = new Vector2();
	}
	
	public String getPartName(){
		return "Abstract Part";
	}
	public int signal(int signal){
		return 0;
	}
	public Vector2 getForceEmitted(float deltaT){
		return new Vector2(0,0);
	}
	
	public PartAdapter getCPUAdapter(){
		return null;
		
	}
	public void update(){
		
	}
	/* (non-Javadoc)
	 * @see com.ulysses.rk.world.entity.Selectable#setHoverState(boolean)
	 */
	@Override
	public void setHoverState(boolean state){
		this.hoveredOn = state;
		for(AnchorPoint a : this.anchors){
			a.setHoverState(state);
		}
	}
	/* (non-Javadoc)
	 * @see com.ulysses.rk.world.entity.Selectable#setSelectionState(boolean)
	 */
	@Override
	public void setSelectionState(boolean state){
		this.selected = state;
		for(AnchorPoint a : this.anchors){
			a.setSelectionState(state);
		}
	}
	public boolean isSelected(){
		return this.selected;
	}
	@Override
	public boolean isHoveredOn(){
		return this.hoveredOn;
	}
	
	public Table getConfigWidgets(){
		return null;		
	}
	
	public void writeToJSON(JsonGenerator jsonGenerator, boolean delimit) throws IOException{
		/*if(delimit)
			jsonGenerator.writeStartObject();
		jsonGenerator.writeStringField("name", getPartName());
		jsonGenerator.writeStringField("id", id.toString()); 
		jsonGenerator.writeNumberField("x", bodylink.getPosition().x); 
		jsonGenerator.writeNumberField("y", bodylink.getPosition().y );
		jsonGenerator.writeNumberField("angle", bodylink.getAngle() );		
		if(delimit)
			jsonGenerator.writeEndObject();*/
	}
	public void readConfigFromJson(JsonParser parser) throws IOException{
		
	}
	
	public void addAnchor(AnchorPoint p){
		anchors.add(p);
	}
	public List<AnchorPoint> getTrussAnchors(){
		return anchors;
	}
	
	
	public void drawLines(ShapeRenderer renderer){
//		renderer.begin(ShapeType.Filled);

//		renderer.end();
	}
	
	
	public void drawAnchors(ShapeRenderer renderer){
		drawTrussAnchors(renderer);
	}
	
	private void drawTrussAnchors(ShapeRenderer renderer){

		renderer.begin(ShapeType.Line);
		for(AnchorPoint a : anchors){
			Color color = Config.getNormalColor();
			if(a.isSelected()){
				color = Config.getSelectedColor();
			}
			if(a.isHoveredOn()){
				color = Config.getHoverColor();
			}		
			renderer.setColor(color);

			Vector2 tmp = this.localToWorld(a.localPosition.cpy());			
			renderer.circle(tmp.x, tmp.y, a.radius*Config.getScaleFactor(),16);

		}
		renderer.end();
		
	}
	
	public AnchorPoint getAnchorAtWorldPos(Vector2 worldCoord){		
		
		for(AnchorPoint a : anchors){
			Vector2 localCoord = worldToLocal(worldCoord);
			if(localCoord.dst(a.localPosition) < a.radius){
				return a;
			}			
		}
		return null;
	}
	
	
	public boolean hasAnchorAt(Vector2 worldCoord){		
		if(getAnchorAtWorldPos(worldCoord)!= null)
			return true;
		else return false;
	}
	
	public List<Integer> getListenForKeys(){
		return listenOn;
	}
	public void setListenOn(List<Integer> listenOn){
		this.listenOn = listenOn;
	}




	/**
	 * Gets the world coordinates of a point defined in the part's local coordinate system
	 * @param
	 */	
	public Vector2 localToWorld(Vector2 localPosition){
		Vector2 result = localPosition.cpy();		
		result.rotate((float) Math.toDegrees(this.getAngleInContainer()));
		result.add(this.getPositionInContainer());
		result.rotate((float) Math.toDegrees(this.getContainerAngle()));
		result.add(this.getContainerPosition());
		
		return result;
	}
	/**
	 * Gets the local coordinates of a point defined in the worlds coordinate system
	 * @param
	 */	
	public Vector2 worldToLocal(Vector2 worldPosition){
		Vector2 result = worldPosition.cpy().sub(getContainerPosition());
		result.rotate((float) Math.toDegrees(-getContainerAngle()));
		result.sub(getPositionInContainer());
		result.rotate((float) Math.toDegrees(-getAngleInContainer()));
		
		return result;
	}
	
	/**
	 * Gets a point defined in the part's local coordinate system in container coordinates
	 * @param
	 */	
	public Vector2 localToContainer(Vector2 localPosition){
		Vector2 result = localPosition.cpy();
		result.rotate((float) Math.toDegrees(this.getAngleInContainer()));	
		result.add(this.getPositionInContainer());
		return result;
	}
	
	/**
	 * Sets the angle of this part, in world coordinates
	 * @param
	 */	
	public float getWorldAngle() {
		return  this.getContainerAngle() + this.getAngleInContainer();
	}
//	
//	/**
//	 * Sets the angle of this part, in world coordinates
//	 * @param
//	 */	
//	public void setWorldAngle(float renderAngle) {
//		this.worldAngle = renderAngle;
//	}
//
	/**
	 * Gets the position of this part, in world coordinates
	 * @param
	 */	
	public Vector2 getWorldPosition() {

		Vector2 result = this.localToWorld(new Vector2());//this.getContainerPosition().cpy().add(this.positionInContainer);
		
		return result;
	}
	
//	/**
//	 * Sets the position of this part, in world coordinates
//	 * @param
//	 */	
//	
//	public void setWorldPosition(Vector2 renderPosition) {
//		this.worldPosition = renderPosition;
//	}

	/**
	 * Gets the position of this part, in it's containers coordinate system 
	 * @param
	 */	
	public Vector2 getPositionInContainer() {
		return positionInContainer;
	}
	
	/**
	 * Sets the position of this part, in it's containers coordinate system 
	 * @param
	 */
	public void setPositionInContainer(Vector2 localPosition) {
		this.positionInContainer = localPosition;
	}
	
	/**
	 * Gets the angle of this part, in it's containers's coordinate system 
	 * @return 
	 */
	public float getAngleInContainer() {
		return angleInContainer;
	}
	
	/**
	 * Sets the angle of this part, in it's container's coordinate system 
	 * @param 
	 */
	public void setAngleInContainer(float localAngle) {
		this.angleInContainer = localAngle;
	}
	
	
	/**
	 * Gets the world position of this part's container
	 * @return
	 */
	public Vector2 getContainerPosition() {
		return containerPosition;
	}

	/**
	 * Sets the world position of this parts's container
	 * @param 
	 */
	public void setContainerPosition(Vector2 containerPosition) {
		this.containerPosition = containerPosition;
	}

	
	/**
	 * Gets the world angle of this part's container 
	 * @return 
	 */
	public float getContainerAngle() {
		return containerAngle;
	}

	/**
	 * Sets the world angle of this part's container 
	 * @return 
	 */
	public void setContainerAngle(float containerAngle) {
		this.containerAngle = containerAngle;
	}

	public String getMass() {
		// TODO Auto-generated method stub
		return "N/A";
	}
}
