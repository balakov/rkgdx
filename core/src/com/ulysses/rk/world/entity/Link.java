package com.ulysses.rk.world.entity;

import java.util.UUID;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

public class Link implements RenderComponent {

	public AnchorPoint a,b;
	UUID id;
	
	public Link(AnchorPoint a, AnchorPoint b){
		this.a=a;
		this.b=b;
		id = UUID.randomUUID();
	}
	
	@Override
	public void draw(SpriteBatch batch, float delta) {
		// TODO Auto-generated method stub

	}

	@Override
	public void drawLines(ShapeRenderer renderer) {
		renderer.setColor(Color.WHITE);
		renderer.begin(ShapeType.Line);
		renderer.line(a.parent.localToWorld(a.localPosition), b.parent.localToWorld(b.localPosition));
		//renderer.line(a.parent.getWorldPosition(), b.parent.getWorldPosition());
		renderer.end();
	}

}
