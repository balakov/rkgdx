package com.ulysses.rk.world.entity;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;

import util.Config;

public class Cockpit extends Part {

	CircleShape circle = new CircleShape();
	public Cockpit(){
		Vector2 av0 = new Vector2(0,2f);

		this.anchors.add(new AnchorPoint(AnchorPoint.TYPE.Truss, av0, 1f, this));		
		Vector2 av1 = new Vector2(-2f,-2f);

		this.anchors.add(new AnchorPoint(AnchorPoint.TYPE.Truss, av1, 1f, this));
		
		Vector2 av2 = new Vector2(2f,-2f);

		this.anchors.add(new AnchorPoint(AnchorPoint.TYPE.Truss, av2, 1f, this));
		
	}
	public List<FixtureDef> getFixtureDefs(Vector2 offset, float angleOffset) {
		
		// main Cockpit circle
		circle.setRadius(3f);
		circle.setPosition(offset);
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = circle;
		fixtureDef.density = 0.5f; 
		fixtureDef.friction = 0.4f;
		fixtureDef.restitution = 0.2f; // Make it bounce a little bit
		List<FixtureDef> fd = new ArrayList<FixtureDef>();
		fd.add(fixtureDef);

		return fd;
	}
	
	
	@Override
	public void finalize(){

		// Remember to dispose of any shapes after you're done with them!
		// BodyDef and FixtureDef don't need disposing, but shapes do.
		circle.dispose();
	}
	@Override
	public void drawLines(ShapeRenderer renderer) {
		renderer.begin(ShapeType.Line);
		Color color = Config.getNormalColor();
		if(this.isSelected()){
			color = Config.getSelectedColor();
		}
		if(this.isHoveredOn()){
			color = Config.getHoverColor();
		}		
		renderer.setColor(color);
		Vector2 position = localToWorld(new Vector2(0,0));
		renderer.circle(position.x, position.y, circle.getRadius(),16);
		renderer.end();
		drawAnchors(renderer);
	}
	@Override
	public void draw(SpriteBatch batch, float delta) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public String getPartName(){
		return("Cockpit");
	}

}
