package com.ulysses.rk.world.entity;


import java.util.UUID;

import com.badlogic.gdx.math.Vector2;

public class AnchorPoint implements Selectable{
	enum TYPE {Truss, Fuel, Power, Struct, Dev};
	TYPE type = TYPE.Truss;
	
	Part parent;
	public UUID id;
	
	public boolean hoveredOn = false;
	public boolean selected = false;
	
	public Vector2 localPosition;
	float radius;
	
	
	public AnchorPoint(TYPE t, Vector2 localCoords, float radius, Part parent){
		this.type = t;
		this.localPosition = localCoords;
		this.radius = radius;
		this.parent = parent;
		
		id = UUID.randomUUID();
	}
	public Part getParentPart(){
		return parent;
	}

	public Vector2 getWorldPosition(){
		return this.parent.localToWorld(localPosition);
	}
	@Override
	public void setHoverState(boolean state) {
		this.hoveredOn = state;
		
	}


	@Override
	public void setSelectionState(boolean state) {
		this.selected = state;
		
	}


	@Override
	public boolean isSelected() {
		return selected;
	}

	@Override
	public boolean isHoveredOn() {
		return hoveredOn;
	}

}
