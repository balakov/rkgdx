package com.ulysses.rk.world.entity;


public interface Selectable {

	void setHoverState(boolean state);

	void setSelectionState(boolean state);

	boolean isSelected();
	
	boolean isHoveredOn();
	

}