package com.ulysses.rk.world.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

public class Truss extends Part{

	UUID part0, part1;
	Vector2 anchor0, anchor1;
	
	PolygonShape shape = new PolygonShape();	
	Vector2 from, to;
	float appliedforce;
	
	public Truss(Vector2 from, Vector2 to, UUID part0, UUID part1, Vector2 local0, Vector2 local1){
		this.from = from;
		this.to = to;
		
		this.part0 = part0;
		this.part1 = part1;
		this.anchor0 = local0;
		this.anchor1 = local1;
		
	}
	@Override
	public void draw(SpriteBatch batch, float delta) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public String getPartName(){
		return "Truss";
	}
	@Override
	public void drawLines(ShapeRenderer renderer) {
		//super.drawLines(renderer);
		
	}
	public void setAppliedForce(float force){
		this.appliedforce = force;
	}
	@Override
	public List<FixtureDef> getFixtureDefs(Vector2 offset, float angleOffset) {

		shape.set(util.Math.makePolygon(from, to, 0.1f)); 
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = shape;
		fixtureDef.density = 0.5f; 
		fixtureDef.friction = 0.4f;
		fixtureDef.restitution = 0.0f; // Make it bounce a little bit
		fixtureDef.filter.groupIndex = -1;
		
		List<FixtureDef> fixtureDefs = new ArrayList<FixtureDef>();
		fixtureDefs.add(fixtureDef);
		return fixtureDefs;
	}

}
