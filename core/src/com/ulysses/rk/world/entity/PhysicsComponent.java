package com.ulysses.rk.world.entity;

import java.util.List;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.FixtureDef;

public interface PhysicsComponent extends Component{
	public List<FixtureDef> getFixtureDefs(Vector2 offset, float angleOffset);
}
