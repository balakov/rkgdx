package com.ulysses.rk.world.entity;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public interface RenderComponent extends Component {
	public void draw(SpriteBatch batch, float delta);
	public void drawLines(ShapeRenderer renderer);
}
