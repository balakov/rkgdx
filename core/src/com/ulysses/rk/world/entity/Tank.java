package com.ulysses.rk.world.entity;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.ulysses.rk.world.entity.AnchorPoint.TYPE;

import resource.Resource;
import util.Config;

public class Tank extends Part {

	public Tank(){
		this.addAnchor(new AnchorPoint(TYPE.Truss, new Vector2(0,-2), 1, this));
		this.addAnchor(new AnchorPoint(TYPE.Truss, new Vector2(0,2), 1, this));
		this.widgetTable = createConfig();
		
	}
	@Override
	public void draw(SpriteBatch batch, float delta) {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void drawLines(ShapeRenderer renderer){
		super.drawAnchors(renderer);
		renderer.begin(ShapeType.Line);
		
		Color color = Config.getNormalColor();
		if(this.isSelected()){
			color = Config.getSelectedColor();
		}
		if(this.isHoveredOn()){
			color = Config.getHoverColor();
		}		
		renderer.setColor(color);
		
		for(Shape s : getShapes()){
			if(s instanceof CircleShape){
				CircleShape c = (CircleShape) s;
				Vector2 center = localToWorld(c.getPosition());
				renderer.circle(center.x, center.y, c.getRadius());
				
			}
			if(s instanceof PolygonShape){
				PolygonShape poly = (PolygonShape) s;
				for(int i=0; i < poly.getVertexCount()-1; i++){
					Vector2 from = new Vector2();
					Vector2 to = new Vector2();
					poly.getVertex(i, from);
					
					if(i<poly.getVertexCount()-1)
						poly.getVertex(i+1, to);
					else poly.getVertex(0,to);
					
					from = localToWorld(from);
					to = localToWorld(to);
					
					renderer.line(from, to);					
				}
				
			}
			
		}
		renderer.end();
	}

	
	@Override
	public List<FixtureDef> getFixtureDefs(Vector2 offset, float angleOffset) {

		CircleShape ct = new CircleShape();
		ct.setRadius(1);
		ct.setPosition(new Vector2(0,-2).rotate((float)Math.toDegrees(angleOffset)).add(offset));
		FixtureDef fixtureDef0 = new FixtureDef();
		fixtureDef0.shape = ct;
		fixtureDef0.density = 0.5f; 
		fixtureDef0.friction = 0.4f;
		fixtureDef0.restitution = 0.6f; 
		
		CircleShape cb = new CircleShape();
		cb.setRadius(1);		
		cb.setPosition(new Vector2(0,2).rotate((float)Math.toDegrees(angleOffset)).add(offset));
		FixtureDef fixtureDef1 = new FixtureDef();
		fixtureDef1.shape = cb;
		fixtureDef1.density = 0.5f; 
		fixtureDef1.friction = 0.4f;
		fixtureDef1.restitution = 0.6f; 
		
		PolygonShape shape = new PolygonShape();		
		Vector2[] vs = new Vector2[4];
		vs[0] = new Vector2(-1f,2);
		vs[1] = new Vector2(-1f,-2);
		vs[2] = new Vector2(1f,-2);
		vs[3] = new Vector2(1f,2);
		
		for(int i=0; i < 4; i++){			
			vs[i].rotate((float)Math.toDegrees(angleOffset));
			vs[i].add(offset);
			
		}		
		
		
		
		shape.set(vs);
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = shape;
		fixtureDef.density = 0.5f; 
		fixtureDef.friction = 0.4f;
		fixtureDef.restitution = 0.6f; 
		
		List<FixtureDef> fd = new ArrayList<FixtureDef>();
		fd.add(fixtureDef);
		fd.add(fixtureDef0);
		fd.add(fixtureDef1);
		return fd;
	}
	
	private ArrayList<Shape> getShapes(){
		ArrayList<Shape> result = new ArrayList<Shape>();
		CircleShape top = new CircleShape();
		top.setRadius(1);
		top.setPosition(new Vector2(0,-2));
		result.add(top);
		CircleShape bottom = new CircleShape();
		bottom.setRadius(1);		
		bottom.setPosition(new Vector2(0,2));
		result.add(bottom);
		PolygonShape trunk = new PolygonShape();
		Vector2[] vs = new Vector2[4];
		vs[0] = new Vector2(-1f,2);
		vs[1] = new Vector2(-1f,-2);
		vs[2] = new Vector2(1f,-2);
		vs[3] = new Vector2(1f,2);	
		trunk.set(vs);
		result.add(trunk);
		return result;
		
	}
	@Override
	public String getPartName(){
		return "Tank";
	}
	
	
	@Override 
	public Table getConfigWidgets(){
		return this.widgetTable;
	}
	
	private Table createConfig(){
		Table t = new Table();
		Label l = new Label("Tank", Resource.getInstance().getDefaultSkin());
		t.add(l);
		return t;
	}
}
