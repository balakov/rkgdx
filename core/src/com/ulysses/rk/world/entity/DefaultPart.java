package com.ulysses.rk.world.entity;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;

import util.Config;

public class DefaultPart extends Part{
	CircleShape circle = new CircleShape();
	
	public DefaultPart(){
		this.anchors.add(new AnchorPoint(AnchorPoint.TYPE.Truss, new Vector2(0,0), 1f, this));		
	}
	
	public List<FixtureDef> getFixtureDefs(Vector2 offset, float angleOffset) {
		// Create a circle shape and set its radius to 6

		circle.setRadius(0f);
		circle.setPosition(offset);
		
		
		
		// Create a fixture definition to apply our shape to
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = circle;
		fixtureDef.density = 0.0f; 
		fixtureDef.friction = 0.4f;
		fixtureDef.restitution = 0.6f; // Make it bounce a little bit
		
		List<FixtureDef> fd = new ArrayList<FixtureDef>();
		fd.add(fixtureDef);
		return fd;
	}
	@Override
	public void finalize(){

		// Remember to dispose of any shapes after you're done with them!
		// BodyDef and FixtureDef don't need disposing, but shapes do.
		circle.dispose();
	}
	@Override
	public void drawLines(ShapeRenderer renderer) {
		renderer.begin(ShapeType.Line);
		for(AnchorPoint a : anchors){
			Color color = Config.getNormalColor();
			if(a.isSelected()){
				color = Config.getSelectedColor();
			}
			if(a.isHoveredOn()){
				color = Config.getHoverColor();
			}		
			renderer.setColor(color);

			Vector2 tmp = this.localToWorld(a.localPosition.cpy());			
			renderer.circle(tmp.x, tmp.y, a.radius*Config.getScaleFactor(),16);

		}
		renderer.end();
	}
	@Override
	public void draw(SpriteBatch batch, float delta) {
		// TODO Auto-generated method stub
	}
	@Override
	public String getPartName(){
		return "DefaultPart";
	}
}
