package com.ulysses.rk.world;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.QueryCallback;

public class FixturePickCallback implements QueryCallback{
	static List<Fixture> fixtures = new ArrayList<Fixture>();
	public FixturePickCallback(){
		fixtures.clear();
	}
	@Override
	public boolean reportFixture(Fixture fixture) {
		fixtures.add(fixture);
		return true;
	}
	
	public List<Fixture> getFixtures(){
		return fixtures;
	}
	
	public List<Fixture> getFixturesPrecise(float x, float y){
		List<Fixture> filtered = new ArrayList<Fixture>();
		for(Fixture f : fixtures){
			if(f.testPoint(x, y)){
				filtered.add(f);
			}
		}
		return filtered;
	}
}
