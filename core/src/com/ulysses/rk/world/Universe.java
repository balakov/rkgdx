package com.ulysses.rk.world;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.jgrapht.alg.ConnectivityInspector;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.Multigraph;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Joint;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.MouseJoint;
import com.badlogic.gdx.physics.box2d.joints.MouseJointDef;
import com.badlogic.gdx.utils.Array;
import com.ulysses.rk.world.entity.AnchorPoint;
import com.ulysses.rk.world.entity.Cockpit;
import com.ulysses.rk.world.entity.Link;
import com.ulysses.rk.world.entity.Part;
import com.ulysses.rk.world.entity.PartContainer;
import com.ulysses.rk.world.entity.PhysicsComponent;
import com.ulysses.rk.world.entity.RenderComponent;
import com.ulysses.rk.world.entity.Selectable;

import tools.ExampleFitPolygon;
import tools.LineDetection;
import util.Config;

public class Universe {

	World world = new World(new Vector2(0, -3), true);
	Box2DDebugRenderer debugRenderer;
	ShapeRenderer renderer = new ShapeRenderer();
	
	Selectable lastPicked = null;
	
	Body groundBody;
	private MouseJoint mouseJoint = null;
	
	HashMap<UUID,PartContainer> partContainers = new HashMap<UUID, PartContainer>();
	

	private float timestep = 1f/120f;
	private int vIterations = 10;
	private int pIterations = 12;

	public Universe(){		
		world.setAutoClearForces(true);
		

	}
	public void doPhysicsStep(float deltaTime) {

		
		world.step(timestep, vIterations, pIterations);

		// sett position according to body
		Array<Body> bodies = new Array<Body>();
		world.getBodies(bodies);
		for(Body b : bodies){
			Object o = b.getUserData();
			if(o instanceof Part){
				Part p = (Part) o;
				p.setContainerPosition(b.getPosition().cpy());
				p.setContainerAngle(b.getAngle());	
				p.update();
			}
		}
		
		for(PartContainer p : partContainers.values()){
			
			p.update();

			
		}
	}

	public void initSoftMove(Part part, Vector2 target){
		Gdx.app.debug("Universe", "Starting soft move to " + target + " Part: " + part);
		MouseJointDef def = new MouseJointDef();
		Body b = getBody(part, false);
		def.bodyA = groundBody;
		def.bodyB = b;
		def.collideConnected = true;
		def.target.set(target.x, target.y);
		def.maxForce = 1000.0f * b.getMass();

		mouseJoint = (MouseJoint)world.createJoint(def);
		b.setAwake(true);	
	}
	public void updateSoftMove(Vector2 target){
		
		if(mouseJoint != null){
			mouseJoint.setTarget(target.set(target.x,target.y));
		}
	}
	public void endSoftMove(){
		if (mouseJoint != null) {
			Gdx.app.debug("Universe", "Soft move ended");
			world.destroyJoint(mouseJoint);
			mouseJoint = null;
		} //else Gdx.app.debug("Universe", "The soft move never ends");
	}
	
	public void draw(SpriteBatch batch, float delta){

		for(PartContainer p : partContainers.values()){
			
			p.render(batch);
		}
		Array<Body> bodies = new Array<Body>();
		world.getBodies(bodies);
		for(Body b : bodies){
			Object o = b.getUserData();
			if(o instanceof RenderComponent){
				RenderComponent t = (RenderComponent) o;
				t.draw(batch,delta);
			}
			
		}
	}
	
	public void debugRender(Camera cam){
		debugRenderer.render(world, cam.combined);

	}
	
	public void lineRender(Camera cam){
		renderer.setProjectionMatrix(cam.combined);
		Array<Body> bodies = new Array<Body>();
		world.getBodies(bodies);
		

		for(Body b : bodies){
			if(b.getType()==BodyType.StaticBody){
				renderer.begin(ShapeType.Line);
				renderer.setColor(Color.WHITE);
				for(Fixture f : b.getFixtureList()){
				/*	PolygonShape s = (PolygonShape)f.getShape();
					for(int i=0; i < s.getVertexCount()-1; i++){
						Vector2 va = new Vector2();
						Vector2 vb = new Vector2();
						s.getVertex(i, va);
						s.getVertex(i+1,vb);

						va = va.add(b.getPosition());
						vb = vb.add(b.getPosition());
						va.scl(Config.getScaleFactor());
						vb.scl(Config.getScaleFactor());
						
						renderer.line(va,vb);
					}
				*/	
				}
				renderer.end();
			}
			

			
			Object o = b.getUserData();
			if(o instanceof Part){
				if(!(o instanceof Cockpit)){
					Part t = (Part) o;
					t.drawLines(renderer);
				}
			}
			
			for(PartContainer p : partContainers.values()){
				
				p.lineRender(renderer);
			}
			
		}
		
		Array<Joint> joints = new Array<Joint>();
		world.getJoints(joints);
		for(Joint j : joints){
			Object o = j.getUserData();
			if(o instanceof RenderComponent){
				RenderComponent t = (RenderComponent) o;
				t.drawLines(renderer);
			}
			
		}
		
		
	}
	public void debugAddGround(){
		
		
		
		
	}
	public Body debugAddObject(float x, float y, Object o){

		if(o instanceof PhysicsComponent){
			PhysicsComponent c = (PhysicsComponent) o;
			// First we create a body definition
			BodyDef bodyDef = new BodyDef();
			bodyDef.type = BodyType.DynamicBody;
			// Set our body's starting position in the world
			bodyDef.position.set(x, y);

			// Create our body in the world using our body definition
			Body body = world.createBody(bodyDef);

			body.setUserData(c);
			for(FixtureDef d : c.getFixtureDefs(new Vector2(0,0),0)){
				body.createFixture(d);	
			}
			PartContainer container = new PartContainer((Part)o, body);	
			partContainers.put(UUID.randomUUID(),container);
			//if(o instanceof Cockpit){
			//	PartContainer container = new PartContainer((Cockpit) o, body);				
			//	partContainers.put(UUID.randomUUID(),container);
			//}
			return body;
		}
		return null;
	}
	private Body addRogueObject(float x, float y, Object o){
		PhysicsComponent c = (PhysicsComponent) o;
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;

		bodyDef.position.set(x, y);
		Body body = world.createBody(bodyDef);

		body.setUserData(c);
		for(FixtureDef d : c.getFixtureDefs(new Vector2(0,0),0)){
			body.createFixture(d);	
		}
		return body;
	}
	
	public boolean hasPartAt(float x, float y){
		if (getPartsAt(x,y).size()==0){
			return false;
		}
		else return true;
	}
	public List<Part> getPartsAt(float x, float y){
		FixturePickCallback callback = new FixturePickCallback();
		world.QueryAABB(callback, x, y, x, y);	

		List<Fixture> fixtures = callback.getFixturesPrecise(x,y);

		List<Part> parts = new ArrayList<Part>();
		for(Fixture f : fixtures){
			
			if(f.getUserData() instanceof Part)
			{
				parts.add((Part) f.getUserData());
	
			}
			// stray Part not in container
			else {
				
				Object p = (Part)f.getBody().getUserData();
				if(p instanceof Part){
					parts.add((Part)p);
				}
			}				
		}
		return parts;		
	}


	public HashMap<UUID, PartContainer> getPartContainer(){
		return this.partContainers;
	}
	
	public PartContainer getPartContainer(UUID id){
		return partContainers.get(id);
	}
	public UUID getContainerID(PartContainer container){
		for(UUID key : partContainers.keySet()){
			if(partContainers.get(key)==container){
				return key;
			}
		}
		return null;
	}
	
	public PartContainer getPartContainer(Part part){
		if(part != null)
		{
			for(PartContainer con : partContainers.values()){
				if(con.containsPart(part.id)){
					return con;
				}
			}
		}
		return null;
	}
	
	public void selectAllParts(boolean state){
		for(Selectable p : this.getAllParts()){
			p.setSelectionState(state);
		}		
	}
	public void hoverAllParts(boolean state){
		for(Selectable p : this.getAllParts()){
			p.setHoverState(state);
		}		
		
	}

	public boolean linkParts(AnchorPoint a, AnchorPoint b){
		Gdx.app.debug("Universe", "Linking " + a.toString() + " : " + b.toString());
		if(a==b){
			Gdx.app.debug("Universe", "Start and end are the same");
			return false;
		}
		PartContainer conA = getPartContainer(a.getParentPart());
		PartContainer conB = getPartContainer(b.getParentPart());
		if(conA==null && conB == null){
			Gdx.app.debug("Universe", "Adding two rogue parts");
			return false;
		}
		
		else if(conA == conB){
			Gdx.app.debug("Universe", "Container interlinking");
			conA.addLink(new Link(a,b));
			return true;
		}
		else if(conA == null && conB != null){
			Gdx.app.debug("Universe", "Adding to b's container");
			Body body = getBody(a.getParentPart(), true);
			conB.addPart(a.getParentPart(), body);
			world.destroyBody(body);
			conB.addLink(new Link(a,b));
			return true;
		}
		else if(conA != null && conB == null){
			Gdx.app.debug("Universe", "Adding to a's container");
			Body body = getBody(b.getParentPart(), true);
			conA.addPart(b.getParentPart(), body);
			world.destroyBody(body);
			conA.addLink(new Link(a,b));
			return true;
		}
		else {
			Gdx.app.debug("Universe", "Merge container");
			
			for(Part p : conB.getAllParts()){
				Gdx.app.debug("Universe", "Adding " + p + " to world");
				float angle = p.getWorldAngle();
				Vector2 pos = p.localToWorld(new Vector2(0,0));
				Body tmp = addRogueObject(pos.x, pos.y, p);
				if(tmp==null)
					System.out.println("FUCKING ASSSS");
				tmp.setTransform(pos, angle);
				conA.addPart(p, tmp);
				world.destroyBody(tmp);
				
			}
			for(Link l : conB.getLinks()){
				conA.addLink(l);
			}
			Gdx.app.debug("Universe", "Removing " + conB);
			world.destroyBody(conB.root);
			
			UUID conId = getContainerID(conB);
			System.out.println("container id: " + conId);
			System.out.println("removed: " + this.partContainers.remove(conId));
			conA.addLink(new Link(a,b));
			return true;
		}

	}
	public void unlink(AnchorPoint a){
		Part p = a.getParentPart();
		PartContainer c = getPartContainer(p);
		if(c!= null){
			c.removeLink(a);
			sanitizeContainer(c);
		}
	}
	
	private void sanitizeContainer(PartContainer c){
		if(c.getAllParts().isEmpty()){
			Gdx.app.debug("Universe", "Container is empty, removing");
			partContainers.remove(getContainerID(c));
			return;
		}
		
		Multigraph<Part, DefaultEdge> g;
		g = new Multigraph<>(DefaultEdge.class);
		for(Part p : c.getAllParts()){
			g.addVertex(p);
		}
		for(Link l : c.getLinks()){
			g.addEdge(l.a.getParentPart(), l.b.getParentPart());
		}
		ConnectivityInspector<Part, DefaultEdge> i = new ConnectivityInspector<>(g);
		if(i.isGraphConnected()){
			Gdx.app.debug("Universe", "Topology good");
		} else {
			
			
			List<Set<Part>> partSets = i.connectedSets();
			Gdx.app.debug("Universe", "Container fragmented into " + partSets.size()+ " set(s) of part(s)");
			Iterator<Set<Part>> iterator = partSets.iterator();
			iterator.next(); // skip first
			while(iterator.hasNext()){
				
				Set<Part> current = iterator.next();
				Gdx.app.debug("Universe", "Found an unconnected set of " + current.size()+" part(s)");
				for(Part p : current){
					float angle = p.getWorldAngle();
					Vector2 pos = p.localToWorld(new Vector2(0,0));
					c.removePart(p);
					Body b = addRogueObject(pos.x, pos.y, p);
					b.setTransform(pos, angle);				
				}				
				Part root = current.iterator().next();
				Gdx.app.debug("Universe", "Creating new Container");
				PartContainer newContainer = new PartContainer(root, getBody(root, false));
				current.remove(root);
				Gdx.app.debug("Universe", "Adding " + current.size()+ " part(s) to newly created container");
				for(Part p : current){
					newContainer.addPart(p, getBody(p, true));					
				}
				this.partContainers.put(UUID.randomUUID(), newContainer);
				
			}
		}
	}
	
	public void removeFromContainer(Part p){
		Gdx.app.debug("Universe", "Removing " + p);		
		PartContainer c = getPartContainer(p);
		if(c!=null){			
			float angle = p.getWorldAngle();
			Vector2 pos = p.localToWorld(new Vector2(0,0));
			c.removePart(p);
			Body b = debugAddObject(pos.x, pos.y, p);
			b.setTransform(pos, angle);
			sanitizeContainer(c);
		} else {
			Gdx.app.debug("Universe", "Container not found " + p);	
		}
		
	}
	
	public boolean deletePart(Part p){
		Gdx.app.debug("Universe", "Deleting " + p);	
		PartContainer owner = getPartContainer(p);
		if(owner!=null){
			owner.removePart(p);
			sanitizeContainer(owner);
		} else {
			world.destroyBody(getBody(p, true));
		}
		return true;
	}	
	
	public boolean deleteAll(){
		for(Part p : this.getAllParts()){
			
			//world.destroyBody(p.bodylink);
			//c.removePart(p);
		}
		for(Link l : this.getAllLinks()){
			//c.removeLink(l);
		}
		
		return true;
	}	
	public void createDebugScene(){
		debugRenderer = new Box2DDebugRenderer();
		debugRenderer.setDrawVelocities(true);
		debugRenderer.setDrawJoints(true);
		debugRenderer.setDrawContacts(true);
		
		//debugRenderer.setDrawJoints(false);
		// Create our body definition
		BodyDef groundBodyDef = new BodyDef();  
		// Set its world position
		groundBodyDef.position.set(new Vector2(-320, -200));  

		// Create a body from the defintion and add it to the world
		groundBody = world.createBody(groundBodyDef);  

		// Create a polygon shape
		PolygonShape groundBox = new PolygonShape();  
		// Set the polygon shape as a box which is twice the size of our view port and 20 high
		// (setAsBox takes half-width and half-height as arguments)
		groundBox.setAsBox(100, 1.0f);
		// Create a fixture from our polygon shape and add it to our ground body  
		groundBody.createFixture(groundBox, 0.0f); 
		
		
		for(ChainShape c : ExampleFitPolygon.getContours("assets/groundtest.png")){
			groundBody.createFixture(c, 0.0f);
		}
		//ChainShape gShape = LineDetection.getLineSegments("assets/groundtest.png");
		//groundBody.createFixture(gShape, 0.0f);
		
		
		// Clean up after ourselves
		groundBox.dispose();
		
		//debugAddObject(0, -20, new Cockpit());
		

		
	}

	public List<Part> getAllParts(){
		ArrayList<Part> all = new ArrayList<Part>();
		Array<Body> bodies = new Array<Body>();
		world.getBodies(bodies);
		for(Body b : bodies){
			Object o = b.getUserData();
			if(o instanceof Part){
				Part p = (Part) o;
				all.add(p);
			}
		}
		
		for(PartContainer container : partContainers.values()){
			if(container!=null){
				for(Part p : container.getAllParts()){
					all.add(p);
				}
			}						
		}
		

		return all;		
	}
	
	public List<Link> getAllLinks(){
		ArrayList<Link> all = new ArrayList<Link>();
		Array<Joint> joints = new Array<Joint>();
		world.getJoints(joints);
		for(Joint j : joints){
			Object o = j.getUserData();
			if(o instanceof Link){
				Link l = (Link) o;
				all.add(l);
			}
		}
		return all;		
	}	
	
	public Set<Part> getAllSelectedParts(){
		List<Part> all = this.getAllParts();
		Set<Part> selected = new HashSet<Part>();
		for(Part p : all){
			if(p.isSelected()){
				selected.add(p);
			}
		}
		return selected;		
	}
	
	public int getNumberOfBodies(){
		return world.getBodyCount();
	}
	public int getNumberOfJoints(){
		return world.getJointCount();
	}
	
	
	private Body getBody(Part p, boolean excludeContainers){
		Array<Body> bodies = new Array<Body>();
		world.getBodies(bodies);
		for(Body b : bodies){
			Object o = b.getUserData();
			if(o instanceof Part){
				if(p==o)
					return b;
			}
			if(!excludeContainers){
				if(o instanceof PartContainer){
					PartContainer container = (PartContainer)o;
					
					if(container.containsPart(p.id))
					return b;
				}				
				
			}

		}
		Gdx.app.debug("Editor", "Part " + p + " - No such body.");
		return null;
	}

}
