package com.ulysses.rk;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.ulysses.rk.states.Editor;

public class Raumkampf extends Game {
    

	
	public BitmapFont defaultFont;
	
	@Override
	public void create () {
		Gdx.app.setLogLevel(Application.LOG_DEBUG);
		this.setScreen(new Editor(this));
		defaultFont = new BitmapFont();
		defaultFont.setColor(Color.WHITE);
		Gdx.graphics.setVSync(false);
		


	}

	@Override
	public void render () {
		super.render(); //important!
	}
	
	@Override
	public void dispose () {

	}
	
	public void drawFPS(){
		Gdx.app.getGraphics().getFramesPerSecond();
	}
}
