package tools;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.util.List;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.ChainShape;

import boofcv.abst.feature.detect.line.DetectLineHoughFoot;
import boofcv.abst.feature.detect.line.DetectLineHoughPolar;
import boofcv.abst.feature.detect.line.DetectLineSegmentsGridRansac;
import boofcv.factory.feature.detect.line.ConfigHoughPolar;
import boofcv.factory.feature.detect.line.FactoryDetectLineAlgs;
import boofcv.gui.ListDisplayPanel;
import boofcv.gui.feature.ImageLinePanel;
import boofcv.gui.image.ShowImages;
import boofcv.io.UtilIO;
import boofcv.io.image.ConvertBufferedImage;
import boofcv.io.image.UtilImageIO;
import boofcv.struct.image.GrayF32;
import boofcv.struct.image.GrayS16;
import boofcv.struct.image.GrayU8;
import boofcv.struct.image.ImageGray;
import georegression.struct.line.LineParametric2D_F32;
import georegression.struct.line.LineSegment2D_F32;
import georegression.struct.point.Point2D_F32;

public class LineDetection {
	 
	// adjusts edge threshold for identifying pixels belonging to a line
	private static final float edgeThreshold = 25;
	// adjust the maximum number of found lines in the image
	private static final int maxLines = 100;
 
	private static ListDisplayPanel listPanel = new ListDisplayPanel();
 
	/**
	 * Detects lines inside the image using different types of Hough detectors
	 *
	 * @param image Input image.
	 * @param imageType Type of image processed by line detector.
	 * @param derivType Type of image derivative.
	 */
	public static<T extends ImageGray, D extends ImageGray>
			void detectLines( BufferedImage image , 
							  Class<T> imageType ,
							  Class<D> derivType )
	{
		// convert the line into a single band image
		T input = ConvertBufferedImage.convertFromSingle(image, null, imageType );
 
		// Comment/uncomment to try a different type of line detector
		DetectLineHoughPolar<T,D> detector = FactoryDetectLineAlgs.houghPolar(
				new ConfigHoughPolar(3, 30, 2, Math.PI / 180,edgeThreshold, maxLines), imageType, derivType);
//		DetectLineHoughFoot<T,D> detector = FactoryDetectLineAlgs.houghFoot(
//				new ConfigHoughFoot(3, 8, 5, edgeThreshold,maxLines), imageType, derivType);
//		DetectLineHoughFootSubimage<T,D> detector = FactoryDetectLineAlgs.houghFootSub(
//				new ConfigHoughFootSubimage(3, 8, 5, edgeThreshold,maxLines, 2, 2), imageType, derivType);
 
		List<LineParametric2D_F32> found = detector.detect(input);
 
		// display the results
		ImageLinePanel gui = new ImageLinePanel();
		gui.setBackground(image);
		gui.setLines(found);
		gui.setPreferredSize(new Dimension(image.getWidth(),image.getHeight()));
 
		listPanel.addItem(gui, "Found Lines");
	}
 
	/**
	 * Detects segments inside the image
	 *
	 * @param image Input image.
	 * @param imageType Type of image processed by line detector.
	 * @param derivType Type of image derivative.
	 */
	public static<T extends ImageGray, D extends ImageGray>
	void detectLineSegments( BufferedImage image ,
							 Class<T> imageType ,
							 Class<D> derivType )
	{
		// convert the line into a single band image
		T input = ConvertBufferedImage.convertFromSingle(image, null, imageType );
 
		// Comment/uncomment to try a different type of line detector
		DetectLineSegmentsGridRansac<T,D> detector = FactoryDetectLineAlgs.lineRansac(10, 20, 2.36, true, imageType, derivType);

		List<LineSegment2D_F32> found = detector.detect(input);
 
		// display the results
		ImageLinePanel gui = new ImageLinePanel();
		gui.setBackground(image);
		gui.setLineSegments(found);
		gui.setPreferredSize(new Dimension(image.getWidth(),image.getHeight()));
 
		listPanel.addItem(gui, "Found Line Segments");
	}
	
	public static ChainShape getLineSegments(String image){
		BufferedImage input = UtilImageIO.loadImage("assets/groundtest.png");
		GrayF32 img = ConvertBufferedImage.convertFromSingle(input, null, GrayF32.class );
		DetectLineSegmentsGridRansac<GrayF32, GrayF32> detector = FactoryDetectLineAlgs.lineRansac(10, 20, 2.36, true, GrayF32.class, GrayF32.class);
		List<LineSegment2D_F32> detectedLines = detector.detect(img);
		
		ChainShape result = new ChainShape();
		Vector2 vectors[] = new Vector2[(detectedLines.size()*2)];
		int vidx = 0;
		for(int i = 0; i < (detectedLines.size()); i++){
			Point2D_F32 a = detectedLines.get(i).getA();
			Point2D_F32 b = detectedLines.get(i).getB();
			vectors[vidx] = new Vector2(a.x,a.y);
			vidx++;
			vectors[vidx] = new Vector2(b.x,b.y);
			vidx++;
		}
		result.createChain(vectors);
		return result;
	}
 
	public static void main( String args[] ) {
		BufferedImage input = UtilImageIO.loadImage("assets/groundtest.png");
 
		detectLines(input, GrayU8.class, GrayS16.class);
 
		// line segment detection is still under development and only works for F32 images right now
		detectLineSegments(input, GrayF32.class, GrayF32.class);
 
		ShowImages.showWindow(listPanel, "Detected Lines", true);
	}
}