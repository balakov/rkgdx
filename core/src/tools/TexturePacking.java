package tools;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
public class TexturePacking
{
    public static void main (String[] args) throws Exception {
        TexturePacker.process("assets/img/control_icons", "assets/img/atlas", "testtex");
    }
}