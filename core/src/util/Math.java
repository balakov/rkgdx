package util;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.PolygonShape;

public class Math {
	public static Vector2[] makePolygon(Vector2 from, Vector2 to, float thickness){
		  float half = thickness/2.0f;
		  
		  Vector2[] vertices = new Vector2[4];
		  		 
		  Vector2 center = from.cpy().interpolate(to.cpy(), 0.5f, Interpolation.linear);


		  Vector2 localFrom = from.cpy().sub(center.cpy());
		  Vector2 localTo = to.cpy().sub(center.cpy());
		  
		  Vector2 fromDown = localFrom.cpy().add(new Vector2(-localFrom.y, localFrom.x).nor().scl(half)); 
		  Vector2 fromUp = localFrom.cpy().add(new Vector2(localFrom.y, -localFrom.x).nor().scl(half)); 

		  Vector2 toDown = localTo.cpy().add(new Vector2(-localTo.y, localTo.x).nor().scl(half)); 
		  Vector2 toUp = localTo.cpy().add(new Vector2(localTo.y, -localTo.x).nor().scl(half)); 
		  
		  vertices[0] = fromDown;
		  vertices[1] = toDown;		 		  
		  vertices[2] = toUp;
		  vertices[3] = fromUp;
		return vertices;
	}
	
	public static String toRoundedString(Vector2 v){
		return "(" +(int)v.x + "|" + (int) v.y+")";
		
	}
	public static String toStringDegrees(float rad){
		return "" + MathUtils.round(MathUtils.radiansToDegrees*rad)+"°";
		
	}
	public static Vector2 alignToGrid(Vector2 in, int gridsize){
		Vector2 out = new Vector2();
		int tmp = MathUtils.round(in.x);
		int rem = tmp%gridsize;
		out.x = tmp-rem;
		tmp = MathUtils.round(in.y);
		rem = tmp%gridsize;
		out.y = tmp-rem;
		return out;
	}
	
	public static Vector2 getCenter(PolygonShape p){ 
		Vector2 result = new Vector2();
		Vector2 tmp = new Vector2();
		for(int i=0; i < p.getVertexCount(); i++){
			p.getVertex(i, tmp);
			result.add(tmp);
		}
		result.x = result.x /  p.getVertexCount();
		result.y = result.y /  p.getVertexCount();
		return result;		
	}
}
