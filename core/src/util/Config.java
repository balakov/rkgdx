package util;


import java.util.Arrays;
import java.util.List;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;

public class Config {
	static List<Integer> allowedPC = Arrays.<Integer>asList(Input.Keys.LEFT,Input.Keys.RIGHT,Input.Keys.UP,Input.Keys.DOWN,Input.Keys.A,Input.Keys.W,Input.Keys.S,Input.Keys.D);
	
	
	public static int getScaleFactor(){
		return 1;
	}
	public static List<Integer> getAllowedKeys(){
		return allowedPC;
	}
	
	public static Color getSelectedColor(){
		return Color.RED;
		
	}
	public static Color getHoverColor(){
		return Color.GREEN;
		
	}
	public static Color getNormalColor(){
		return Color.WHITE;
		
	}
	public static Color getPoweredColor(){
		return Color.GOLDENROD;
	}
	
}
