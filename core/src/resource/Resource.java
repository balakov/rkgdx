package resource;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class Resource {

	public BitmapFont defaultFont;
	Skin uiSkin = new Skin(Gdx.files.internal("assets/data/uiskin.json"));
	AssetManager assetManager;
	
	static Resource instance;
	
	private Resource(){
		defaultFont = new BitmapFont();
		defaultFont.setColor(Color.WHITE);
		

	};
	
	public static Resource getInstance(){
		if(instance == null){
			instance = new Resource();
		}
		return instance;
	}
	
	public  Skin getDefaultSkin(){
		return uiSkin;
	}
	
	public BitmapFont getDefaultFont(){
		return defaultFont;
	}
	
	public  AssetManager getAssetManager(){
		if(assetManager == null){
			assetManager = new AssetManager();
			
			String cpuAtlas = "assets/img/atlas/testtex.atlas";
			assetManager.load(cpuAtlas, TextureAtlas.class);
			assetManager.finishLoading();

		}
		return assetManager;
	}
	
	public TextureAtlas getCPUAtlas(){
		return this.getAssetManager().get("assets/img/atlas/testtex.atlas");
		
	}
	
}
