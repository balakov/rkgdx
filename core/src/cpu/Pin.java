package cpu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Align;

import resource.Resource;

public class Pin extends BoardComponent{
	enum TYPE{IN,OUT};
	TYPE type;
	
	Sprite sprite;
	public Pin(TYPE type){
		this.type = type;
		this.setWidth(4);
		this.setHeight(16);
		this.setPosition(0, 0, Align.center);
		this.setName("Pin");
    	sprite = Resource.getInstance().getCPUAtlas().createSprite("pin");
		if(sprite==null)
			Gdx.app.debug("Pin", "Could not load Sprite");
		
	}
	
    @Override
    public void draw (Batch batch, float parentAlpha) {
    	
    	batch.end();
    	if(this.active || isHoveredOn()){
    		sr.setColor(Color.RED);
    	}
    		
    	else {
    		sr.setColor(Color.WHITE);
    		}
    	
    	
    	sr.begin(ShapeType.Line);
    	Vector2 tmp = localToStageCoordinates(new Vector2(0,0));
    	sr.rect(tmp.x,tmp.y, getWidth(), getHeight());
    	sr.end();
    	batch.begin();
        //batch.draw(sprite, getX(), getY(), getOriginX(), getOriginY(),
        //    getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
    }
}
