package cpu;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;

import cpu.Pin.TYPE;
import resource.Resource;

public class KeyInputComponent extends BoardComponent {

	Integer listenOn;
	public KeyInputComponent(Integer listenOn){
		
		
		Pin pin0 = new Pin(TYPE.OUT);
		pin0.setPosition(0, -10);
		this.addActor(pin0);
		
		Pin pin1 = new Pin(TYPE.OUT);
		pin1.setPosition(10, -10);
		this.addActor(pin1);
		
		Pin pin2 = new Pin(TYPE.OUT);
		pin2.setPosition(20, -10);
		this.addActor(pin2);
		
		Pin pin3 = new Pin(TYPE.OUT);
		pin3.setPosition(30, -10);
		this.addActor(pin3);
		
		this.setName("KeyInput");
		this.listenOn = listenOn;
		
		
	}
	public void draw (Batch batch, float parentAlpha) {
    	
    	if(this.active){
    		batch.setColor(Color.RED);
    		sr.setColor(Color.RED);
    	}
    		
    	else {
    		batch.setColor(Color.WHITE);
    		sr.setColor(Color.WHITE);
    		}
    	super.draw(batch, parentAlpha);
    	
        Resource.getInstance().getDefaultFont().draw(batch, Input.Keys.toString(listenOn), this.getX() + this.getOriginX(), this.getY() + this.getOriginY());
        
        
    	batch.end();
        sr.begin(ShapeType.Line);
        if(this.isSelected()){
        	
        }
    	Vector2 tmp = localToStageCoordinates(new Vector2(0,0));
    	sr.rect(tmp.x,tmp.y, getWidth(), getHeight());

    	
    	sr.end();
    	batch.begin();
    }
	
	public Integer getListenOn(){
		return listenOn;
	}
	
}
