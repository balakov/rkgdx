package cpu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Group;

import util.Config;

public class CircuitBoard extends Group{
	HashMap<UUID, BoardComponent> inputs;
	HashMap<UUID, BoardComponent> components;
	HashMap<UUID, BoardComponent> outputs; 
	
	
	ArrayList<Wire> wires;
	
	private int adapterX = 20;
	private int adapterY = 0;
	
	public CircuitBoard(){
		components = new HashMap<UUID, BoardComponent>();
		wires = new ArrayList<Wire>();
		outputs = new HashMap<UUID,BoardComponent>();
		inputs = new HashMap<UUID,BoardComponent>();
		generateInputComponents();
	}
	
	public void addComponent(BoardComponent component){
		components.put(UUID.randomUUID() ,component);
		this.addActor(component);
		
	}
	public void addOutputAdapter(PartAdapter a){
		if(a!=null)
		{
			a.setX(adapterX);		
			a.setY(20);
			adapterX+=40;
			outputs.put(a.getPartId(), a);
			this.addActor(a);
		} else Gdx.app.debug("CircuitBoard", "No Part Adapter");
	}
	
	
	public void removeBuiltIn(UUID partId){
		BoardComponent c = outputs.get(partId);
		this.removeActor(c);
		outputs.remove(partId);
		
	}
	
	public Wire linkComponents(Pin a, Pin b){
		Gdx.app.debug("CircuitBoard", "Linking " + a + " and " +b);
		Wire w = new Wire(a,b);
		wires.add(w);
		this.addActor(w);
		return w;
		
	}
	public void setInput(ArrayList<Integer> input){
		for(BoardComponent c : inputs.values()){
			if(c instanceof KeyInputComponent){
				KeyInputComponent k = (KeyInputComponent) c;
				if(input.contains(k.getListenOn()))
				{
					k.setActive(true);
				}
				else {
					k.setActive(false);
				}
			}
			
			
		}
		solve();
	}
	private void solve(){
		resetBoard();
		for(BoardComponent c : inputs.values()){
			if(c.active){
				powerConnected(c);
				
			}			
		}

		
	}
	
	private void resetBoard(){
		for(BoardComponent c : components.values() ){
			c.setActive(false);
		}
		for(BoardComponent c : outputs.values() ){
			c.setActive(false);
		}
		for(Wire w : wires){
			w.setActive(false);
		}
		
	}
	
	private void powerConnected(BoardComponent component){
		for(Wire w : wires){
			BoardComponent from = (BoardComponent) w.from.getParent();
			BoardComponent to = (BoardComponent) w.to.getParent();
			if(from == component || to == component){
				to.setActive(true);
				from.setActive(true);
				w.setActive(true);
			}
		}		
	}
	
	
	private void generateInputComponents(){
		int x = 20;
		for(Integer i : Config.getAllowedKeys()){
			KeyInputComponent k = new KeyInputComponent(i);
			k.setX(x);
			k.setY(100);
			inputs.put(UUID.randomUUID(),k);
			x+=40;
			this.addActor(k);
		}		
	}
	
	public int getSignal(UUID partid){
		BoardComponent c = outputs.get(partid);
		if(c!=null){		
			if (c.active)
				return 1;
			else return 0;
		}
		return 0;
	}
	
	public HashMap<UUID, BoardComponent> getComponents(){
		return components;
	}
	
	public HashMap<UUID, BoardComponent> getInputs(){
		return inputs;
	}
	
	public HashMap<UUID, BoardComponent> getOutputs(){
		return outputs;
	}
	
	public ArrayList<Wire> getWires(){
		return this.wires;
	}
	
	public Wire getWire(Pin pin){
		for(Wire w : wires){
			if(w.from == pin || w.to == pin)
				return w;
		}
		return null;
	}
	public boolean isWired(Pin pin){
		if(getWire(pin)==null)
			return false;
		else return true;
	}
	

	public void deselectAll() {
		for(Wire w : wires){
			w.setSelectionState(false);
		}
		
	}
	
	private void unHoverAll(){
		for(BoardComponent c : outputs.values()){
			c.setHoverState(false);
		}
	}
	


}
