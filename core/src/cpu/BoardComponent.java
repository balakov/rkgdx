package cpu;

import java.util.UUID;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Align;
import com.ulysses.rk.world.entity.Selectable;

import resource.Resource;

public abstract class BoardComponent extends Group implements Selectable 
{
	AssetManager am = new AssetManager();
	TextureAtlas atlas;
	Sprite sprite;
	
	UUID id;
	ShapeRenderer sr;
	private boolean selected;
	boolean active;
	private boolean hover;
	
	public BoardComponent(){
		init();
		active = false;
		this.setBounds(0, 0, 32, 32);
		this.setOrigin(Align.center);
		this.setName("Generic BoardComponent");
		sr = new ShapeRenderer();
	}

	private void init(){
		
		
		atlas = Resource.getInstance().getAssetManager().get("assets/img/atlas/testtex.atlas");
		//AtlasRegion region = atlas.findRegion("imagename");
		sprite = atlas.createSprite("keyinput");
		if(sprite==null)
			Gdx.app.debug("BoardComponent", "Could not load Sprite");
	}	
    @Override
    public void draw (Batch batch, float parentAlpha) {  
         super.draw(batch, parentAlpha);
         //batch.draw(sprite, getX(), getY(), getOriginX(), getOriginY(),
         //   getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
    	
/*
    	sr.setColor(Color.WHITE);
 
    	sr.begin(ShapeType.Line);
    	sr.rect(getX(), getY(), getWidth(), getHeight());
    	sr.circle(this.getX(), this.getY(), 5);
    	sr.end();
  */     
        
    }
    
    public void setActive(boolean state){
    	this.active = state;
    }
    
    public boolean isActive(){
    	return active;
    }

	public boolean isSelected() {
		return selected;
	}

	public void setSelectionState(boolean selected) {
		this.selected = selected;
	}

	public boolean isHoveredOn() {
		return hover;
	}

	public void setHoverState(boolean hover) {
		this.hover = hover;
	}
}
