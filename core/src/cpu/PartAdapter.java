package cpu;

import java.util.UUID;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;

import cpu.Pin.TYPE;
import resource.Resource;

public class PartAdapter extends BoardComponent {
	UUID id;
	public PartAdapter(UUID belongingPartId){
		this.id = belongingPartId;
		
		Pin pin0 = new Pin(TYPE.IN);
		pin0.setPosition(0, 20);
		this.addActor(pin0);
		
		Pin pin1 = new Pin(TYPE.IN);
		pin1.setPosition(10, 20);
		this.addActor(pin1);
		
		Pin pin2 = new Pin(TYPE.IN);
		pin2.setPosition(20, 20);
		this.addActor(pin2);
		
		Pin pin3 = new Pin(TYPE.IN);
		pin3.setPosition(30, 20);
		this.addActor(pin3);
		this.setName("PartAdapter");
		
	}
	
	public UUID getPartId(){
		return id;
	}
	
	public void draw (Batch batch, float parentAlpha) {
    	
		batch.end();
    	if(this.active || isHoveredOn()){
    		sr.setColor(Color.RED);
    	}
    		
    	else {
    		sr.setColor(Color.WHITE);
    		}
    	
    	
    	sr.begin(ShapeType.Line);
    	Vector2 tmp = localToStageCoordinates(new Vector2(0,0));
    	sr.rect(tmp.x,tmp.y, getWidth(), getHeight());
    	sr.end();
    	batch.begin();
        Resource.getInstance().getDefaultFont().draw(batch, "PartAdapter", this.getX(), this.getY());
    	super.draw(batch, parentAlpha);
        
    }
}
