package cpu;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Align;

import util.Config;

public class Wire extends BoardComponent{

	public Pin from, to;
	
	public Wire(Pin from, Pin to){
		this.from = from;
		this.to = to;
		

	
		this.setName("Wire");

	}
	
	public void update(){
		/*Vector2 center = new Vector2();
		
		
		Actor win = from.getParent().getParent();
		
		Vector2 fPos = from.localToAscendantCoordinates(win, new Vector2(from.getX(), from.getY()));
		Vector2 tPos = to.localToAscendantCoordinates(win, new Vector2(to.getX(), to.getY()));
		
		center.x = fPos.x + 0.5f*(tPos.x-fPos.x);
		center.y = fPos.y + 0.5f*(tPos.y-fPos.y);
		

		float distance = tPos.dst(fPos);
		
		Vector2 tmp = tPos.cpy().sub(fPos);
		float rotation = (float) Math.atan2(tmp.y,tmp.x); 
		this.setOrigin(Align.center);
		
		this.setX(center.x + 5);
		this.setY(center.y - distance/2);
		this.setWidth(10);
		this.setHeight(distance);
		//this.setHeight(10);
		
		this.setRotation((float) Math.toDegrees(rotation)+90);

*/
	}
	
    public void draw (Batch batch, float parentAlpha) {
    	
    	sr.setColor(Config.getNormalColor());
    	if(this.active){
    		sr.setColor(Config.getPoweredColor());
    	}
    	if(this.isSelected()){
    		sr.setColor(Config.getSelectedColor());
    	}
 /*      batch.draw(sprite, getX(), getY(), getOriginX(), getOriginY(),
            getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
        super.draw(batch, parentAlpha);
  */      
        batch.end();
        sr.begin(ShapeType.Line);
	        
			Vector2 fPos = from.localToStageCoordinates(new Vector2(0,0));
			Vector2 tPos = to.localToStageCoordinates(new Vector2(0,0));
			sr.line(fPos, tPos);
			sr.end();
        batch.begin();
        
    }
}
